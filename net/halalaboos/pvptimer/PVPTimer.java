package net.halalaboos.pvptimer;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.commands.CommandHandler;
import net.halalaboos.plugin.util.PlayerMetadataHandler;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.purgatory.events.PurgatoryEnterEvent;
import net.halalaboos.purgatory.events.PurgatoryExitEvent;
import net.halalaboos.pvptimer.command.Enable;
import net.halalaboos.pvptimer.events.ProtectionEvent;
import net.halalaboos.pvptimer.events.ProtectionEvent.ProtectionType;

public class PVPTimer extends BasePlugin implements Listener {
			
	protected int defaultDuration = 1800000;
	
	public final PlayerMetadataHandler<Long> protectionHandler = new PlayerMetadataHandler<Long>(this, "pvp_protection") {

		@Override
		protected void withoutMetadata(Player player) {
			player.setMetadata(handleId, new FixedMetadataValue(plugin, PluginUtils.getTime()));
			plugin.sendNotificationMessage(player, "You are under pvp protection for 15 minutes!");
		}
		
		@Override
		protected void withMetadata(Player player) {
			if (!hasProtectionDisabled(player)) {
				long length = getProtectionLength(player);
	    		int duration = getProtectionDuration(player);
		    	if (length >= duration) {
		    		plugin.sendNotificationMessage(player, "You are no longer under pvp protection!");
		    		setProtectionDisabled(player);
		    	} else
		    		plugin.sendNotificationMessage(player, "You have " + getProtectionTime(player) + " of pvp protection!");
			}
		}

		@Override
		public MetadataValue getMetadataValue(Long value) {
			return new FixedMetadataValue(plugin, value);
		}
		
		@Override
		public Long getMetadata(Player player) {
			List<MetadataValue> values = player.getMetadata(handleId);
	    	return values.get(0).asLong();
		}
		
		@Override
		public boolean applyMetadata(Player player, Long value) {
			player.setMetadata(handleId, getMetadataValue(value));
			return true;
		}
		
	};
	
	public PVPTimer() {
		super("PVPTimer", "pvptimer.default", "pvptimer.admin");
	}

	@Override
	protected void enable() {
		this.getServer().getPluginManager().registerEvents(this, this);
		this.getServer().getPluginManager().registerEvents(protectionHandler, this);
		protectionHandler.load(getConfig());
		
		CommandHandler pvpToggleHandler = new CommandHandler("PVP", this);
		pvpToggleHandler.loadCommand(new Enable(this));
       	this.getCommand("pvp").setExecutor(pvpToggleHandler);

	}

	@Override
	protected void disable() {
		protectionHandler.copyAllMetadata();
		protectionHandler.save(getConfig());
		this.saveConfig();
	}

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemPickup(PlayerPickupItemEvent event) {
    	if (protectionHandler.hasMetadata(event.getPlayer())) {
    		if (hasProtectionDisabled(event.getPlayer()))
    			return;
    		long length = this.getProtectionLength(event.getPlayer());
    		int duration = getProtectionDuration(event.getPlayer());
    		if (length < duration) {
	    		event.setCancelled(true);
	    	} else if (length >= duration) {
	    		this.setProtectionDisabled(event.getPlayer());
	    	}
	    }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPreprocess(PlayerCommandPreprocessEvent event) {
    	if (event.getMessage().startsWith("/f home") || event.getMessage().startsWith("/factions home")) {
	    	if (protectionHandler.hasMetadata(event.getPlayer())) {
	    		if (hasProtectionDisabled(event.getPlayer()))
	    			return;
	    		long length = this.getProtectionLength(event.getPlayer());
	    		int duration = getProtectionDuration(event.getPlayer());
		    	if (length < duration) {
		    		event.setCancelled(true);
		    		sendErrorMessage(event.getPlayer(), "You are under pvp protection! Type '/pvp on' to enabled pvp. (" + getProtectionTime(event.getPlayer()) + ")");
		    	} else if (length >= duration) {
		    		this.setProtectionDisabled(event.getPlayer());
		    	}
		    }
    	}
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamageEntity(EntityDamageByEntityEvent event) {
    	if (event.getDamager() instanceof Player) {
	        if (protectionHandler.hasMetadata((Player) event.getDamager())) {
	        	if (hasProtectionDisabled((Player) event.getDamager()))
	    			return;
	        	long length = this.getProtectionLength((Player) event.getDamager());
	    		int duration = getProtectionDuration((Player) event.getDamager());
	    		if (length < duration) {
	        		event.setCancelled(true);
	        	} else if (length >= duration) {
		    		this.setProtectionDisabled((Player) event.getDamager());
		    	}
	        }
    	}
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDamaged(EntityDamageEvent event) {
    	if (event.getEntity() instanceof Player) {
	        if (protectionHandler.hasMetadata((Player) event.getEntity())) {
	        	if (hasProtectionDisabled((Player) event.getEntity()))
	    			return;
	        	long length = this.getProtectionLength((Player) event.getEntity());
	    		int duration = getProtectionDuration((Player) event.getEntity());
		    	if (length < duration) {
		    		event.setCancelled(true);
		    	} else if (length >= duration) {
		    		this.setProtectionDisabled((Player) event.getEntity());
		    	}
	        }
    	}
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPurgatoryExit(PurgatoryExitEvent event) {
    	ProtectionEvent protectionEvent = new ProtectionEvent(event.getPlayer(), event.getPlayer().getUniqueId(), ProtectionType.ENABLED);
		Bukkit.getPluginManager().callEvent(protectionEvent);
		if (!protectionEvent.isCancelled()) {
			protectionHandler.applyMetadata(event.getPlayer(), PluginUtils.getTime());
			this.sendNotificationMessage(event.getPlayer(), "You are under pvp protection for 15 minutes!");
		}
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPurgatoryEnter(PurgatoryEnterEvent event) {
    	if (event.getPlayer() != null) {
	    	if (protectionHandler.hasMetadata(event.getPlayer())) {
	    		long length = this.getProtectionLength(event.getPlayer());
	    		int duration = getProtectionDuration(event.getPlayer());
		    	if (length < duration) {
		    		this.setProtectionDisabled(event.getPlayer());
		    	}
	    	}
    	} else {
    		if (protectionHandler.hasMetadata(event.getOfflinePlayer().getUniqueId())) {
    			long length = this.getProtectionLength(event.getOfflinePlayer().getUniqueId());
	    		int duration = getProtectionDuration(event.getOfflinePlayer().getUniqueId());
		    	if (length < duration) {
		    		this.setProtectionDisabled(event.getOfflinePlayer().getUniqueId());
		    	}
    		}
    	}
    }
    
    public void setProtectionDisabled(Player player) {
		ProtectionEvent protectionEvent = new ProtectionEvent(player, player.getUniqueId(), ProtectionType.DISABLED);
		Bukkit.getPluginManager().callEvent(protectionEvent);
		if (!protectionEvent.isCancelled())
			protectionHandler.applyMetadata(player, -1000000000000000001L);
    }
    
    public void setProtectionDisabled(UUID uuid) {
		ProtectionEvent protectionEvent = new ProtectionEvent(null, uuid, ProtectionType.DISABLED);
		Bukkit.getPluginManager().callEvent(protectionEvent);
		if (!protectionEvent.isCancelled())
			protectionHandler.putMetadata(uuid, -1000000000000000001L);
    }
    
    
    public boolean hasProtectionDisabled(Player player) {
    	return protectionHandler.getMetadata(player) == -1000000000000000001L;
    }
    
    public String getProtectionTime(Player player) {
		long banLength = (defaultDuration - getProtectionLength(player));
		return PluginUtils.getTime(banLength);
	}
    
    public long getProtectionLength(Player player) {
    	long metadata = protectionHandler.getMetadata(player);
    	if (metadata == -1)
    		return metadata;
    	return PluginUtils.getTime() - metadata;
    }
    
    public long getProtectionLength(UUID uuid) {
    	long metadata = protectionHandler.getMetadata(uuid);
    	if (metadata == -1)
    		return metadata;
    	return PluginUtils.getTime() - metadata;
    }

    public int getProtectionDuration(UUID uuid) {
    	return this.defaultDuration;
    }
    
    public int getProtectionDuration(Player player) {
    	return this.defaultDuration;
    }
    
    public boolean hasProtection(Player player) {
    	long length = this.getProtectionLength(player);
		int duration = getProtectionDuration(player);
    	return length < duration;
    }
}
