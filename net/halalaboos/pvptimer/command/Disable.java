package net.halalaboos.pvptimer.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.pvptimer.PVPTimer;

public class Disable implements InternalCommand {

	private final PVPTimer pvpTimer;
	
	public Disable(PVPTimer pvpTimer)  {
		this.pvpTimer = pvpTimer;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "off", "disable" };
	}

	@Override
	public String[] getHelp() {
		return new String[] {};
	}

	@Override
	public String getDescription() {
		return "Disables pvp";
	}

	@Override
	public Use getUse() {
		return Use.PLAYER;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		pvpTimer.protectionHandler.applyMetadata(player, PluginUtils.getTime());
		pvpTimer.sendConfirmMessage(player, "PVP now disabled!");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(pvpTimer.getDefaultPermission());
	}

}
