package net.halalaboos.pvptimer.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.pvptimer.PVPTimer;

public class Enable implements InternalCommand {

	private final PVPTimer pvpTimer;
	
	public Enable(PVPTimer pvpTimer)  {
		this.pvpTimer = pvpTimer;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "on", "enable" };
	}

	@Override
	public String[] getHelp() {
		return new String[] {};
	}

	@Override
	public String getDescription() {
		return "Enables pvp";
	}

	@Override
	public Use getUse() {
		return Use.PLAYER;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		if (pvpTimer.hasProtection(player)) {
			pvpTimer.setProtectionDisabled(player);
			pvpTimer.sendConfirmMessage(player, "PVP now enabled!");
		}
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(pvpTimer.getDefaultPermission());
	}

}
