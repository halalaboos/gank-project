package net.halalaboos.purgatory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.commands.CommandHandler;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.plugin.util.Position;
import net.halalaboos.purgatory.events.PurgatoryEnterEvent;
import net.halalaboos.purgatory.events.PurgatoryExitEvent;
import net.halalaboos.purgatory.killmessage.KillMessage;
import net.halalaboos.purgatory.lives.LivesHandler;
import net.halalaboos.purgatory.settings.PurgatorySet;
import net.halalaboos.purgatory.settings.PurgatorySetExit;
import net.halalaboos.purgatory.settings.PurgatorySize;
import net.minelink.ctplus.event.NpcDespawnEvent;
import net.minelink.ctplus.event.NpcDespawnReason;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.world.WorldInitEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Purgatory extends BasePlugin implements Listener {

	private final Map<String, Ban> banList = new HashMap<String, Ban>();
	private final Map<String, Integer> livesList = new HashMap<String, Integer>();
	private final Map<String, String> killMessageList = new HashMap<String, String>();

	private FileConfiguration bansConfig = null;
	private File bansFile = null;
	
	private FileConfiguration livesConfig = null;
	private File livesFile = null;
	
	private Position purgatory = new Position(0, 0, 0, 0, 0, ""), exit = new Position(0, 0, 0, 0, 0, "");
	private int purgatorySize = 20;
	
	private BanLength[] banLengths = new BanLength[] {
		new BanLength("donator3", 3600000),
		new BanLength("donator2", 7200000),
		new BanLength("donator1", 10800000),
		new BanLength("default", 14400000),
	};
	
	public Purgatory() {
		super("Purgatory", "purgatory.default", "purgatory.admin");
		
	}
	
	@Override
	protected void enable() {
       	ConfigurationSerialization.registerClass(Ban.class);
		this.saveResource("bans.yml", false);
		this.saveResource("lives.yml", false);

       	bansFile = new File(getDataFolder(), "bans.yml");
		bansConfig = YamlConfiguration.loadConfiguration(bansFile);
		loadBans();
		
		livesFile = new File(getDataFolder(), "lives.yml");
		livesConfig = YamlConfiguration.loadConfiguration(livesFile);
		loadLives();
		
		this.loadConfig();
		
		this.getServer().getPluginManager().registerEvents(this, this);
       	this.getCommand("lives").setExecutor(new LivesHandler(this));
       	
       	CommandHandler settingsHandler = new CommandHandler("Purgatory", this);
       	settingsHandler.loadCommand(new PurgatorySet(this));
       	settingsHandler.loadCommand(new PurgatorySetExit(this));
       	settingsHandler.loadCommand(new PurgatorySize(this));
       	this.getCommand("purgatory").setExecutor(settingsHandler);
       	this.getCommand("killmessage").setExecutor(new KillMessage(this));
    }

	@Override
	protected void disable() {
		this.saveDefaultConfig();
		this.getConfig().set("purgatory", purgatory);
		this.getConfig().set("exit", exit);
		this.getConfig().set("purgatorySize", purgatorySize);
		for (BanLength banLength : banLengths) {
			this.getConfig().set("bans." + banLength.name, banLength.getDuration());
		}
		this.saveConfig();
		saveBans();
		saveLives();
	}
	
	private void loadConfig() {
		try {
			this.purgatory = (Position) getConfig().get("purgatory");
			this.exit = (Position) getConfig().get("exit");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (purgatory == null) {
			purgatory = new Position(0, 0, 0, 0, 0, "");
			this.getLogger().warning("Purgatory location set to x: 0, y: 0, z: 0.");
		}
		if (exit == null) {
			exit = new Position(0, 0, 0, 0, 0, "");
			this.getLogger().warning("Exit location set to x: 0, y: 0, z: 0.");
		}
		this.purgatorySize = getConfig().getInt("purgatorySize");

		for (BanLength banLength : banLengths) {
			banLength.setDuration(getConfig().getInt("bans." + banLength.name));
		}
	}
	
	private void loadLives() {
		Map<String, Object> loaded = livesConfig.getConfigurationSection("lives").getValues(false);
		if (loaded != null) {
			Set<String> keys = loaded.keySet();
			for (String uuid : keys) {
				int lives = (int) loaded.get(uuid);
				livesList.put(uuid, lives);
			}
		}
	}
	
	private void saveLives() {
		livesConfig.createSection("lives", livesList);
		try {
			livesConfig.save(livesFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadBans() {
		Map<String, Object> loaded = bansConfig.getConfigurationSection("bans").getValues(false);
		if (loaded != null) {
			Set<String> keys = loaded.keySet();
			for (String uuid : keys) {
				Ban ban = (Ban) loaded.get(uuid);
				banList.put(uuid, ban);
			}
		}
	}
	
	private void saveBans() {
		bansConfig.createSection("bans", banList);
		try {
			bansConfig.save(bansFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onWorldLoad(WorldInitEvent event) {
		purgatory.updateWorld(event.getWorld());
		exit.updateWorld(event.getWorld());
	}

	/*@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (player.getHealth() - event.getDamage() <= 0) {
				PurgatoryEnterEvent enterEvent = new PurgatoryEnterEvent(player);
				Bukkit.getServer().getPluginManager().callEvent(enterEvent);
				if (!enterEvent.isCancelled()) {
					ban(player);
					event.setCancelled(true);
					ban(player);
					player.getWorld().strikeLightningEffect(player.getLocation());
					PluginUtils.removeAllPotionEffects(player);
					PluginUtils.dropInventory(player, player.getWorld(), player.getLocation());
					PluginUtils.dropArmor(player, player.getWorld(), player.getLocation());
					player.updateInventory();
					player.setHealth(20);
					player.teleport(purgatory.toLocation());
					sendNotificationMessage(player, "You are now in purgatory!");
				}
			}
		}
	}*/
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onNpcDespawn(NpcDespawnEvent event) {
		if (event.getDespawnReason() == NpcDespawnReason.DEATH) {
			Player entity = event.getNpc().getEntity();
			UUID uuid = event.getNpc().getIdentity().getId();
			OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
			if (player != null) {
				PurgatoryEnterEvent enterEvent = new PurgatoryEnterEvent(null, player);
				Bukkit.getServer().getPluginManager().callEvent(enterEvent);
				if (!enterEvent.isCancelled()) {
					this.ban(player.getUniqueId());
					entity.getWorld().strikeLightningEffect(entity.getLocation());
					if (entity.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
						EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) entity.getLastDamageCause();
						Bukkit.broadcastMessage(String.format("%s%s %swas sent to Purgatory by %s%s", ChatColor.RED, event.getNpc().getIdentity().getName(), ChatColor.YELLOW, ChatColor.RED, damageEvent.getDamager().getName()));
					} else {
						Bukkit.broadcastMessage(String.format("%s%s %swent to hell.", ChatColor.RED, event.getNpc().getIdentity().getName(), ChatColor.YELLOW));
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(PlayerDeathEvent event) {
		PurgatoryEnterEvent enterEvent = new PurgatoryEnterEvent(event.getEntity(), null);
		Bukkit.getServer().getPluginManager().callEvent(enterEvent);
		if (!enterEvent.isCancelled()) {
			ban(event.getEntity());
			event.getEntity().getWorld().strikeLightningEffect(event.getEntity().getLocation());
			if (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();
				Bukkit.broadcastMessage(String.format("%s%s %swas sent to Purgatory by %s%s.", ChatColor.RED, event.getEntity().getName(), ChatColor.YELLOW, ChatColor.RED, damageEvent.getDamager().getName()));
			} else {
				Bukkit.broadcastMessage(String.format("%s%s %swent to hell.", ChatColor.RED, event.getEntity().getName(), ChatColor.YELLOW));
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		if (isBanned(event.getPlayer()) && !checkBan(event.getPlayer())) {
			event.setRespawnLocation(purgatory.toLocation());
			sendNotificationMessage(event.getPlayer(), "You are now in purgatory!");
		} else {
			this.unban(event.getPlayer());
			PurgatoryExitEvent exitEvent = new PurgatoryExitEvent(event.getPlayer());
			Bukkit.getServer().getPluginManager().callEvent(exitEvent);
			if (!exitEvent.isCancelled()) {
				event.setRespawnLocation(exit.toLocation());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (isBanned(event.getPlayer())) {
			if (checkBan(event.getPlayer())) {
				this.unban(event.getPlayer());
				this.sendToExit(event.getPlayer());
				return;
			}
			if (!event.getPlayer().isDead())
				sendNotificationMessage(event.getPlayer(), "You're banned for " + getBanTime(event.getPlayer()) + ".");
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerMove(PlayerMoveEvent event) {
		if (isBanned(event.getPlayer()) && purgatory != null) {
			if (checkBan(event.getPlayer())) {
				this.unban(event.getPlayer());
				this.sendToExit(event.getPlayer());
				return;
			}
			if (!event.getPlayer().isDead()) {
				Location position = event.getTo();
				double distX = purgatory.getX() - position.getX();
				double distZ = purgatory.getZ() - position.getZ();
				if ((distX >= purgatorySize || distX <= -purgatorySize) || (distZ >= purgatorySize || distZ <= -purgatorySize)) {
					event.setTo(purgatory.toLocation());
					sendNotificationMessage(event.getPlayer(), "Time left: " + getBanTime(event.getPlayer()));
				}
			}
		}
	}
	
	private boolean checkBan(Player player) {
		return getBanLength(player) >= banList.get(player.getUniqueId().toString()).duration;
	}

	public String getBanTime(Player player) {
		long banLength = (getBanInformation(player).duration - getBanLength(player));
			return PluginUtils.getTime(banLength);
	}
	
	public long getBanLength(String uuid) {
		return (PluginUtils.getTime() - banList.get(uuid).banTime);
	}
	
	public long getBanLength(UUID uuid) {
		return getBanLength(uuid.toString());
	}
	
	public long getBanLength(Player player) {
		return getBanLength(player.getUniqueId().toString());
	}
	
	public int getBanDuration(Player player) {
		int banDuration = 0;
		for (BanLength banLength : banLengths) {
			if (player.hasPermission("purgatory." + banLength.name)) {
				banDuration = banLength.getDuration();
				break;
			}
		}
		return banDuration;
	}
	
	public int getBanDuration(UUID uuid) {
		int banDuration = 0;
		PermissionUser user = PermissionsEx.getPermissionManager().getUser(uuid);
		for (BanLength banLength : banLengths) {
			if (user.has("purgatory." + banLength.name)) {
				banDuration = banLength.getDuration();
				break;
			}
		}
		return banDuration;
	}
	
	public Ban getBanInformation(String uuid) {
		return banList.get(uuid);
	}
	
	public Ban getBanInformation(UUID uuid) {
		return getBanInformation(uuid.toString());
	}
	
	public Ban getBanInformation(Player player) {
		return getBanInformation(player.getUniqueId().toString());
	}
	
	public boolean isBanned(String uuid) {
		return banList.containsKey(uuid);
	}
	
	public boolean isBanned(UUID uuid) {
		return banList.containsKey(uuid.toString());
	}
	
	public boolean isBanned(Player player) {
		return banList.containsKey(player.getUniqueId().toString());
	}
	
	public void unban(String uuid) {
		banList.remove(uuid);
	}
	
	public void unban(UUID uuid) {
		unban(uuid.toString());
	}
	
	public void unban(Player player) {
		unban(player.getUniqueId().toString());
	}
	
	public void ban(UUID uuid) {
		banList.put(uuid.toString(), new Ban(PluginUtils.getTime(), getBanDuration(uuid)));
	}
	
	public void ban(Player player) {
		banList.put(player.getUniqueId().toString(), new Ban(PluginUtils.getTime(), getBanDuration(player)));
	}
	
	public boolean hasLives(String uuid) {
		return livesList.containsKey(uuid) ? livesList.get(uuid) > 0 : false;
	}
	
	public boolean hasLives(UUID uuid) {
		return hasLives(uuid.toString());
	}
	
	public boolean hasLives(Player player) {
		return hasLives(player.getUniqueId().toString());
	}
	
	public int getLives(String uuid) {
		return hasLives(uuid) ? livesList.get(uuid) : 0;
	}
	
	public int getLives(UUID uuid) {
		return getLives(uuid.toString());
	}
	
	public int getLives(Player player) {
		return getLives(player.getUniqueId().toString());
	}
	
	public void addLives(String uuid, int lives) {
		int count = 0;
		if (livesList.containsKey(uuid)) {
			count = livesList.get(uuid);
		}
		livesList.put(uuid, count + lives);
	}
	
	public void addLives(UUID uuid, int lives) {
		addLives(uuid.toString(), lives);
	}
	
	public void addLives(Player player, int lives) {
		addLives(player.getUniqueId().toString(), lives);
	}
	
	public void removeLives(String uuid) {
		livesList.remove(uuid);
	}
	
	public void removeLives(UUID uuid) {
		removeLives(uuid.toString());
	}
	
	public void removeLives(Player player) {
		removeLives(player.getUniqueId().toString());
	}
	
	public void removeLives(String uuid, int lives) {
		int count = 0;
		if (livesList.containsKey(uuid)) {
			count = livesList.get(uuid);
		}
		livesList.put(uuid, count - lives);
	}
	
	public void removeLives(UUID uuid, int lives) {
		removeLives(uuid.toString(), lives);
	}
	
	public void removeLives(Player player, int lives) {
		removeLives(player.getUniqueId().toString(), lives);
	}
	
	public void setLives(String uuid, int lives) {
		livesList.put(uuid, lives);
	}
	
	public void setLives(UUID uuid, int lives) {
		setLives(uuid.toString(), lives);
	}
	
	public void setLives(Player player, int lives) {
		setLives(player.getUniqueId().toString(), lives);
	}

	public Position getExit() {
		return exit;
	}

	public void setExit(Location location) {
		this.exit.setX(location.getX());
		this.exit.setY(location.getY());
		this.exit.setZ(location.getZ());
		this.exit.setYaw(location.getYaw());
		this.exit.setPitch(location.getPitch());
		this.exit.setWorld(location.getWorld());
	}

	public void setLocation(Location location) {
		this.purgatory.setX(location.getX());
		this.purgatory.setY(location.getY());
		this.purgatory.setZ(location.getZ());
		this.purgatory.setYaw(location.getYaw());
		this.purgatory.setPitch(location.getPitch());
		this.purgatory.setWorld(location.getWorld());
	}
	
	public void sendToExit(Player player) {
		PurgatoryExitEvent event = new PurgatoryExitEvent(player);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (!event.isCancelled()) {
			player.teleport(exit.toLocation());
		}
	}
	
	public Position getLocation() {
		return purgatory;
	}

	public void setSize(int size) {
		this.purgatorySize = size;
	}
	
	public int getSize() {
		return purgatorySize;
	}
}
