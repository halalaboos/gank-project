package net.halalaboos.purgatory;

public class BanLength {

	public final String name;
	
	private int duration;
	
	public BanLength(String name, int duration) {
		this.name = name;
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
}
