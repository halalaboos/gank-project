package net.halalaboos.purgatory.lives;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.purgatory.Purgatory;

public class LivesTransfer implements InternalCommand {

	private final Purgatory purgatory;
	
	public LivesTransfer(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "transfer", "t" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "transfer <player> <amount>" };
	}

	@Override
	public String getDescription() {
		return "Transfer a life to another player.";
	}
	
	@Override
	public Use getUse() {
		return Use.PLAYER;
	}
	
	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		if (args.length >= 3 && PluginUtils.isInteger(args[2])) {
			int transferAmount = Integer.parseInt(args[2]);
			Player transferPlayer = PluginUtils.getPlayer(args[1]);
			if (transferPlayer != null) {
				if (transferAmount > 0) {
					if (purgatory.hasLives(player)) {
						int amount = purgatory.getLives(player);
						if (transferAmount <= amount) {
							purgatory.addLives(transferPlayer, transferAmount);
							purgatory.removeLives(player, transferAmount);
							purgatory.sendConfirmMessage(transferPlayer, String.format("%s has sent you %s lives!", player.getDisplayName(), "" + transferAmount));
							purgatory.sendConfirmMessage(player, String.format("Transferred %s lives to %s.", "" + transferAmount, transferPlayer.getDisplayName()));
						} else
							purgatory.sendErrorMessage(player, "You do not have enough lives!");
					} else
						purgatory.sendErrorMessage(player, "You do not have any lives!");
				} else
					purgatory.sendErrorMessage(player, "'" + transferAmount + "' is not a valid amount!");

			} else
				purgatory.sendErrorMessage(player, "'" + args[1] + "' could not be found!");
		} else 
			purgatory.sendErrorMessage(player, "Usage: /lives transfer <player> <amount>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getDefaultPermission());
	}

}
