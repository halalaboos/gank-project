package net.halalaboos.purgatory.lives;

import net.halalaboos.plugin.commands.CommandHandler;
import net.halalaboos.purgatory.Purgatory;

public class LivesHandler extends CommandHandler {
	
	
	public LivesHandler(Purgatory purgatory) {
		super("Lives", purgatory);
		this.loadCommand(new LivesUse(purgatory));
		this.loadCommand(new LivesAdd(purgatory));
		this.loadCommand(new LivesList(purgatory));
		this.loadCommand(new LivesTransfer(purgatory));
	}

}
