package net.halalaboos.purgatory.lives;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.purgatory.Purgatory;

public class LivesList implements InternalCommand {

	private final Purgatory purgatory;
	
	public LivesList(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "list", "l" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "List your current lives.";
	}
	
	@Override
	public Use getUse() {
		return Use.PLAYER;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		purgatory.sendNotificationMessage(sender, "Lives: " + purgatory.getLives(player));
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getDefaultPermission());
	}

}
