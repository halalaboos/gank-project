package net.halalaboos.purgatory.lives;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.purgatory.Purgatory;

public class LivesUse implements InternalCommand {

	private final Purgatory purgatory;
	
	public LivesUse(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "use", "u" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Use a life to revive yourself.";
	}
	
	@Override
	public Use getUse() {
		return Use.PLAYER;
	}
	
	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		if (purgatory.isBanned(player)) {
			if (purgatory.hasLives(player)) {
				purgatory.removeLives(player, 1);
				purgatory.unban(player);
				purgatory.sendToExit(player);
				purgatory.sendConfirmMessage(sender, "Welcome back! " + purgatory.getLives(player) + " lives left.");
				if (purgatory.getLives(player) <= 0)
					purgatory.removeLives(player);
			} else
				purgatory.sendErrorMessage(sender, "No lives left!");
		} else 
			purgatory.sendErrorMessage(sender, "You are not dead.");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getDefaultPermission());
	}

}
