package net.halalaboos.purgatory.lives;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evilmidget38.UUIDFetcher;

import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.purgatory.Purgatory;

public class LivesAdd implements InternalCommand {

	private final Purgatory purgatory;
	
	public LivesAdd(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "add", "a" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "add <username> <value>" };
	}

	@Override
	public String getDescription() {
		return "Add lives to a player.";
	}
	
	@Override
	public Use getUse() {
		return Use.BOTH;
	}
	
	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 3 && PluginUtils.isInteger(args[2])) {
			try {
				UUID uuid;
				Player player = PluginUtils.getPlayer(args[1]);
				if (player != null)
					uuid = player.getUniqueId();
				else
					uuid = UUIDFetcher.getUUIDOf(args[1]);
				purgatory.addLives(uuid, Integer.parseInt(args[2]));
				purgatory.sendConfirmMessage(sender, args[1] + " now has " + purgatory.getLives(uuid) + " lives.");
			} catch (Exception e) {
				e.printStackTrace();
				purgatory.sendErrorMessage(sender, "Unabled to connect to mojang servers. Reason: " + e.getMessage());

			}
		} else
			purgatory.sendNotificationMessage(sender, "Usage: /lives add <player> <value>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getAdminPermissions());
	}
	
}
