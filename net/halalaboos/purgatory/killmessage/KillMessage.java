package net.halalaboos.purgatory.killmessage;

import net.halalaboos.purgatory.Purgatory;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KillMessage implements CommandExecutor {

	private static final String[] DONATORS = {
		"admin",
		"donator3",
		"donator2",
		"donator1"
	};
	
	private final Purgatory purgatory;
	
	public KillMessage(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			boolean hasMe = label.contains("%me%");
			boolean hasUser = label.contains("%user%");
			if (checkDonator(player)) {
				
			}
		}
		return true;
	}
	
	private boolean checkDonator(Player player) {
		for (String donator : DONATORS) {
			if (player.hasPermission("purgatory." + donator)) {
				return true;
			}
		}
		return false;
	}

}
