package net.halalaboos.purgatory.settings;

import org.bukkit.command.CommandSender;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;
import net.halalaboos.purgatory.Purgatory;

public class PurgatorySize implements InternalCommand {

	private final Purgatory purgatory;
	
	public PurgatorySize(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "size", "si" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Set the size of the purgatory.";
	}
	
	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 2 && PluginUtils.isInteger(args[1])) {
			purgatory.setSize(Integer.parseInt(args[1]));
			purgatory.sendConfirmMessage(sender, "Purgatory size set to '" + purgatory.getSize() + "'.");
		} else
			purgatory.sendNotificationMessage(sender, "Usage: /purgatory size <value>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getAdminPermissions());
	}

}
