package net.halalaboos.purgatory.settings;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.purgatory.Purgatory;

public class PurgatorySetExit implements InternalCommand {

	private final Purgatory purgatory;
	
	public PurgatorySetExit(Purgatory purgatory) {
		this.purgatory = purgatory;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "setexit" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Set the location of the purgatory exit.";
	}
	
	@Override
	public Use getUse() {
		return Use.PLAYER;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		Player player = (Player) sender;
		Location location = player.getLocation();
		this.purgatory.setExit(location);
		purgatory.sendConfirmMessage(sender, "Exit set to x: " + location.getBlockX() + ", y: " + location.getBlockY() + ", z: " + location.getBlockZ());
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(purgatory.getAdminPermissions());
	}

}
