package net.halalaboos.purgatory;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class Ban implements ConfigurationSerializable {

	public final long banTime;
	
	public final int duration;
	
	public Ban(Map<String, Object> map) {
		this((long) map.get("banTime"), (int) map.get("duration"));
	}
	
	public Ban(long banTime, int duration) {
		this.banTime = banTime;
		this.duration = duration;
	}
	
	@Override
	public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("banTime", banTime);
        map.put("duration", duration);
		return map;
	}
	
	public static Ban deserialize(Map<String, Object> map) {
		return new Ban((long) map.get("banTime"), (int) map.get("duration"));
	}
	
	public static Ban valueOf(Map<String, Object> map) {
		return new Ban((long) map.get("banTime"), (int) map.get("duration"));
	}
	
}
