package net.halalaboos.family;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class ChameleonData implements ConfigurationSerializable {
	
	public static final Material[] HELMETS = { Material.LEATHER_HELMET, Material.GOLD_HELMET, Material.CHAINMAIL_HELMET, Material.IRON_HELMET, Material.DIAMOND_HELMET };
	
	public static final Material[] CHESTPLATES = { Material.LEATHER_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.IRON_CHESTPLATE, Material.DIAMOND_CHESTPLATE };

	public static final Material[] LEGGINGS = { Material.LEATHER_LEGGINGS, Material.GOLD_LEGGINGS, Material.CHAINMAIL_LEGGINGS, Material.IRON_LEGGINGS, Material.DIAMOND_LEGGINGS };

	public static final Material[] BOOTS = { Material.LEATHER_BOOTS, Material.GOLD_BOOTS, Material.CHAINMAIL_BOOTS, Material.IRON_BOOTS, Material.DIAMOND_BOOTS };
	
	/**
	 * -1 = none
	 *  0 = leather
	 *  1 = gold
	 *  2 = chainmail
	 *  3 = iron
	 *  4 = diamond
	 * */
	private int[] armorStatus = { 2, 2, 2, 2 };
	
	private boolean enabled = false;
	
	public ChameleonData(Map<String, Object> map) {
		this((boolean) map.get("enabled"), (int) map.get("0"), (int) map.get("1"), (int) map.get("2"), (int) map.get("3"));
	}
	
	public ChameleonData(boolean enabled, int... armorStatus) {
		this.enabled = enabled;
		this.armorStatus = armorStatus;
	}
	
	public ChameleonData() {
		this.armorStatus = new int[] { 2, 2, 2, 2 };
	}
	
	public Material getArmor(int slot) {
		switch (slot) {
		case 0:
			return HELMETS[armorStatus[slot]];
		case 1:
			return CHESTPLATES[armorStatus[slot]];
		case 2:
			return LEGGINGS[armorStatus[slot]];
		case 3:
			return BOOTS[armorStatus[slot]];
		default:
			return null;
		}
	}
	
	public void nextArmor(int slot) {
		armorStatus[slot]++;
		if (armorStatus[slot] > 4) {
			armorStatus[slot] = 0;
		}
	}
	
	public void noArmor(int slot) {
		armorStatus[slot] = -1;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("enabled", enabled);
		map.put("0", armorStatus[0]);
		map.put("1", armorStatus[1]);
		map.put("2", armorStatus[2]);
		map.put("3", armorStatus[3]);
		return map;
	}
	
	public static ChameleonData deserialize(Map<String, Object> map) {
		return new ChameleonData((boolean) map.get("enabled"), (int) map.get("0"), (int) map.get("1"), (int) map.get("2"), (int) map.get("3"));
	}
	
	public static ChameleonData valueOf(Map<String, Object> map) {
		return new ChameleonData((boolean) map.get("enabled"), (int) map.get("0"), (int) map.get("1"), (int) map.get("2"), (int) map.get("3"));
	}
	
	public void toggle() {
		enabled = !enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
