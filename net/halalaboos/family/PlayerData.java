package net.halalaboos.family;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

public class PlayerData implements ConfigurationSerializable {

	private final UUID uuid;
	
	private GameClass currentClass;
	
	private boolean enabled;
	
	public PlayerData(Map<String, Object> map) {
		this((String) map.get("uuid"), (int) map.get("classId"));
	}
	
	public PlayerData(Player player) {
		this(player.getUniqueId(), null);
	}
	
	public PlayerData(String uuid, int classId) {
		this(UUID.fromString(uuid), FamilyFriendlyClasses.getClassFromId(classId));
	}
	
	public PlayerData(UUID uuid, GameClass currentClass) {
		this.uuid = uuid;
		this.currentClass = currentClass;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uuid", uuid.toString());
		map.put("classId", FamilyFriendlyClasses.getIdFromClass(currentClass));
		return map;
	}

	public synchronized GameClass getCurrentClass() {
		return currentClass;
	}

	public synchronized boolean hasClass() {
		return currentClass != null;
	}
	
	public synchronized void setCurrentClass(GameClass currentClass) {
		this.currentClass = currentClass;
		this.enabled = false;
	}

	public synchronized boolean isClassEnabled() {
		return enabled;
	}

	public synchronized void setClassEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public static PlayerData deserialize(Map<String, Object> map) {
		return new PlayerData((String) map.get("uuid"), (int) map.get("classId"));
	}
	
	public static PlayerData valueOf(Map<String, Object> map) {
		return new PlayerData((String) map.get("uuid"), (int) map.get("classId"));
	}
}
