package net.halalaboos.family;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import com.codingforcookies.armorequip.ArmorEquipEvent;

import net.halalaboos.family.classes.AssassinClass;
import net.halalaboos.family.classes.BardClass;
import net.halalaboos.family.classes.WorkerClass;
import net.halalaboos.family.classes.chameleon.ChameleonClass;
import net.halalaboos.family.tasks.ClassEquipTask;
import net.halalaboos.family.util.ClassUtil;
import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.settings.SimpleFileConfig;
import net.halalaboos.plugin.util.PlayerMetadataHandler;
import net.halalaboos.pvptimer.PVPTimer;
import net.halalaboos.pvptimer.events.ProtectionEvent;
import net.halalaboos.pvptimer.events.ProtectionEvent.ProtectionType;

public class FamilyFriendlyClasses extends BasePlugin implements Listener {

	public static final List<GameClass> CLASSES = new ArrayList<GameClass>();
	
	private final PlayerMetadataHandler<ChameleonData> chameleonDataHandler = new PlayerMetadataHandler<ChameleonData>(this, "chameleon_data") {

		@Override
		protected void withoutMetadata(Player player) {
		}

		@Override
		protected void withMetadata(Player player) {
		}

		@Override
		public MetadataValue getMetadataValue(ChameleonData value) {
			return new FixedMetadataValue(plugin, value);
		}

		@Override
		public ChameleonData getMetadata(Player player) {
			List<MetadataValue> values = player.getMetadata(handleId);
	    	return (ChameleonData) values.get(0).value();
		}

		@Override
		public boolean applyMetadata(Player player, ChameleonData value) {
			player.setMetadata(handleId, getMetadataValue(value));
			return true;
		}
		
	};
	
	private final PlayerMetadataHandler<PlayerData> playerDataHandler = new PlayerMetadataHandler<PlayerData>(this, "class_data") {

		@Override
		protected void withoutMetadata(Player player) {
			PlayerData data = new PlayerData(player);
			applyMetadata(player, data);
		}

		@Override
		protected void withMetadata(Player player) {
		}

		@Override
		public MetadataValue getMetadataValue(PlayerData value) {
			return new FixedMetadataValue(plugin, value);
		}

		@Override
		public PlayerData getMetadata(Player player) {
			List<MetadataValue> values = player.getMetadata(handleId);
	    	return (PlayerData) values.get(0).value();
		}

		@Override
		public boolean applyMetadata(Player player, PlayerData value) {
			player.setMetadata(handleId, getMetadataValue(value));
			return true;
		}
		
	};
	
	private SimpleFileConfig chameleonConfig, playerConfig;
		
	private PVPTimer pvpTimer;
	
	public FamilyFriendlyClasses() {
		super("Family Friendly Classes", "classes.default", "classes.admin");
	}

	@Override
	protected void enable() {
       	ConfigurationSerialization.registerClass(ChameleonData.class);
       	ConfigurationSerialization.registerClass(PlayerData.class);
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(chameleonDataHandler, this);
		Bukkit.getPluginManager().registerEvents(playerDataHandler, this);
		loadClasses();
		enableClasses();
		
		pvpTimer = (PVPTimer) Bukkit.getPluginManager().getPlugin("PVPTimer");
		
		chameleonConfig = new SimpleFileConfig(this, new File(getDataFolder(), "chameleon.yml"));
		chameleonConfig.load();
		chameleonDataHandler.load(chameleonConfig.getFileConfiguration());
		
		playerConfig = new SimpleFileConfig(this, new File(getDataFolder(), "players.yml"));
		playerConfig.load();
		playerDataHandler.load(playerConfig.getFileConfiguration());
	}
	
	@Override
	protected void disable() {
		disableClasses();
		
		chameleonDataHandler.copyAllMetadata();
		chameleonDataHandler.save(chameleonConfig.getFileConfiguration());
		chameleonConfig.save();
		
		playerDataHandler.copyAllMetadata();
		playerDataHandler.save(playerConfig.getFileConfiguration());
		playerConfig.save();
	}
	
	private void enableClasses() {
		for (GameClass gameClass : CLASSES) {
			gameClass.enable();
		}
	}
	
	private void disableClasses() {
		for (GameClass gameClass : CLASSES) {
			gameClass.disable();
		}
	}
	
	private void loadClasses() {
		CLASSES.add(new WorkerClass(this));
		CLASSES.add(new BardClass(this));
		CLASSES.add(new ChameleonClass(this));
		CLASSES.add(new AssassinClass(this));
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerEquipArmor(ArmorEquipEvent event) {
		if (pvpTimer.protectionHandler.hasMetadata(event.getPlayer()) && pvpTimer.hasProtection(event.getPlayer()))
			return;
		boolean hasNewArmor = event.getNewArmorPiece() != null && event.getNewArmorPiece().getType() != Material.AIR;
		boolean hasOldArmor = event.getOldArmorPiece() != null && event.getOldArmorPiece().getType() != Material.AIR;
		Player player = event.getPlayer();
		int armorIndex = event.getType().ordinal();
		GameClass playerClass = null;
		
		if (hasOldArmor) {
			playerClass = getClassWithItem(player, event.getOldArmorPiece(), armorIndex);
		}
		if (hasNewArmor) {
			GameClass itemClass = getClass(event.getNewArmorPiece());
			if (playerClass != null && playerClass != itemClass) {
				removePlayerClass(event.getPlayer(), playerClass);
				return;
			} else {
				if (itemClass != null && itemClass.hasClassWithItem(player, event.getNewArmorPiece(), armorIndex)) {
					runEquipTask(player, itemClass);
				}
			}
		} else {
			if (hasOldArmor && playerClass != null) {
				if (playerClass.hasClassWithItem(player, event.getOldArmorPiece(), armorIndex)) {
					removePlayerClass(event.getPlayer(), playerClass);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		clearPlayerEffects(event.getPlayer());
		
		for (GameClass gameClass : CLASSES) {
			if (gameClass.hasClass(event.getPlayer())) {
				runEquipTask(event.getPlayer(), gameClass);
				break;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
		GameClass gameClass = getClass(event.getPlayer());
		if (gameClass != null)
			removePlayerClass(event.getPlayer(), gameClass);
    }
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerKick(PlayerKickEvent event) {
		GameClass gameClass = getClass(event.getPlayer());
		if (gameClass != null)
			removePlayerClass(event.getPlayer(), gameClass);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerProtection(ProtectionEvent event) {
		if (event.getPlayer() != null) {
			GameClass gameClass = getClass(event.getPlayer());
			if (event.getType() == ProtectionType.ENABLED) {
				if (gameClass != null)
					removePlayerClass(event.getPlayer(), gameClass);
			} else {
				if (gameClass != null)
					runEquipTask(event.getPlayer(), gameClass);
			}
		}
	}

	public void removePlayerClass(Player player, GameClass gameClass) {
		PlayerData playerData = this.playerDataHandler.getMetadata(player);
		gameClass.onArmourUnEquip(player);
		playerData.setCurrentClass(null);
	}
	
	public void runEquipTask(Player player, GameClass class_) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new ClassEquipTask(this, player, class_), 100);
	}
	
	public void clearPlayerEffects(Player player) {
		for (GameClass gameClass : CLASSES) {
			gameClass.clearPlayerEffects(player);
		}
	}
	
	public GameClass getClassWithItem(Player player, ItemStack item, int armorIndex) {
		for (GameClass gameClass : CLASSES) {
			if (gameClass.hasClassWithItem(player, item, armorIndex)) {
				return gameClass;
			}
		}
		return null;
	}
	
	public GameClass getClass(ItemStack item) {
		for (GameClass gameClass : CLASSES) {
			if (gameClass.isClassArmor(item)) {
				return gameClass;
			}
		}
		return null;
	}
	
	public GameClass getClass(Player player) {
		if (this.playerDataHandler.hasMetadata(player)) {
			PlayerData playerData = this.playerDataHandler.getMetadata(player);
			return playerData.getCurrentClass();
		} else
			return null;
	}

	public PlayerMetadataHandler<ChameleonData> getChameleonDataHandler() {
		return chameleonDataHandler;
	}
	
	public PlayerMetadataHandler<PlayerData> getPlayerDataHandler() {
		return playerDataHandler;
	}

	public static GameClass getClassFromId(int id) {
		if (id >= 0 && id < CLASSES.size())
			return CLASSES.get(id);
		else
			return null;
	}
	
	public static int getIdFromClass(GameClass gameClass) {
		for (int i = 0; i < CLASSES.size(); i++) {
			if (CLASSES.get(i) == gameClass)
				return i;
		}
		return -1; /* what! */
	}
	
}
