package net.halalaboos.family.classes;

import net.halalaboos.plugin.util.PluginUtils;

public class ArcherTag {

	private final long time;
	
	private final float amount;
	
	private final int duration;
	
	public ArcherTag(float amount, int duration) {
		this.time = PluginUtils.getTime();
		this.amount = amount;
		this.duration = duration;
	}
	
	public float getAmount() {
		return amount;
	}
	
	public boolean isActive() {
		return (PluginUtils.getTime() - time) <= duration;
	}
	
}
