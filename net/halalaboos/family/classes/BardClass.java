package net.halalaboos.family.classes;

import net.halalaboos.family.GameClass;
import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.tasks.BroadcastPotionTask;
import net.halalaboos.family.util.ClassUtil;
import net.halalaboos.plugin.Default;

import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffectType;

import pw.gank.factions.api.Faction;
import pw.gank.factions.api.Member;
import pw.gank.factions.impl.bukkit.core.FactionsPlugin;

public class BardClass extends GameClass implements Listener {
	
	public BardClass(FamilyFriendlyClasses classes) {
		super(classes, "Bard", new Material[] { Material.GOLD_HELMET, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.GOLD_BOOTS });
	}

	@Override
	public void enable() {
		Bukkit.getPluginManager().registerEvents(this, classes);
	}
	
	@Override
	public void disable() {
	}
	
	@Override
	public void onArmourEquip(Player player) {
		ClassUtil.addInfinitePotionEffect(PotionEffectType.WEAKNESS, 1, player);
		ClassUtil.addInfinitePotionEffect(PotionEffectType.SPEED, 1, player);
		classes.sendNotificationMessage(player, "You are now a bard.");
	}

	@Override
	public void onArmourUnEquip(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.WEAKNESS, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.SPEED, player);
	}

	@Override
	public void clearPlayerEffects(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.WEAKNESS, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.SPEED, player);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
		boolean isPotion = event.getItem().getType() == Material.SPLASH_POTION;
		if (isPotion) {
			PotionMeta metaData = (PotionMeta) event.getItem().getItemMeta();
			if (this.hasClassEnabled(event.getPlayer())) {
				FactionsPlugin factions = Default.instance.getFactions();
				Optional<Member> factionOptional = factions.getBoard().getMember(event.getPlayer().getUniqueId());
				if (factionOptional.isPresent()) {
					Faction faction = factionOptional.get().getFaction();
					BroadcastPotionTask task = new BroadcastPotionTask(classes, faction, event.getPlayer(), event.getItem(), metaData);
					Bukkit.getScheduler().scheduleSyncDelayedTask(classes, task);
				}
			}
		}
	}
	
}
