package net.halalaboos.family.classes.chameleon;


import net.halalaboos.family.ChameleonData;
import net.halalaboos.family.GameClass;
import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.util.ClassUtil;
import net.halalaboos.plugin.util.InventoryMenu;
import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class ChameleonClass extends GameClass implements Listener {
	
	private ChameleonEquipmentSpoofer equipmentSpoofer;
	
	public ChameleonClass(FamilyFriendlyClasses classes) {
		super(classes, "Chameleon", new Material[] { Material.CHAINMAIL_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS });
	}

	@Override
	public void enable() {
		Bukkit.getPluginManager().registerEvents(this, classes);
		// equipmentSpoofer = new ChameleonEquipmentSpoofer(this, classes);
	}
	
	@Override
	public void disable() {
	}

	@Override
	public void onArmourEquip(Player player) {
		ClassUtil.addInfinitePotionEffect(PotionEffectType.NIGHT_VISION, 3, player);
		ClassUtil.addInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, 3, player);
		// this.equipmentSpoofer.updateAllArmor(player, classes.getChameleonDataHandler().getMetadata(player));
		classes.sendNotificationMessage(player, "You are now a chameleon.");
	}

	@Override
	public void onArmourUnEquip(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.NIGHT_VISION, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, player);
		// this.equipmentSpoofer.updateAllArmor(player, classes.getChameleonDataHandler().getMetadata(player));
	}

	@Override
	public void clearPlayerEffects(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.NIGHT_VISION, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, player);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerUse(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
//		boolean isRightClick = (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK);
//		boolean isSword = event.getItem() != null && (event.getItem().getType() == Material.DIAMOND_SWORD || 
//				event.getItem().getType() == Material.IRON_SWORD ||
//				event.getItem().getType() == Material.WOOD_SWORD || event.getItem().getType() == Material.STONE_SWORD);
//		if (isRightClick && isSword) {
//			player.hidePlayer(player);
//		}
//		boolean isLeftClick = (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK);
//		boolean isBook = event.getItem() != null && event.getItem().getType() == Material.BOOK;
//		if (this.hasClassEnabled(player) && isLeftClick && isBook) {
//			if (!classes.getChameleonDataHandler().hasMetadata(player)) {
//				classes.getChameleonDataHandler().applyMetadata(player, new ChameleonData());
//			}
//			InventoryMenu menu = generateMenu(player);
//			menu.open(player);
//		}
	}
	
	protected InventoryMenu generateMenu(Player player) {
		ChameleonData chameleonData = classes.getChameleonDataHandler().getMetadata(player);
		
		InventoryMenu menu = new InventoryMenu(classes, "Armor Spoof", 27) {

			@Override
			protected void onClickItem(Player player, InventoryClickEvent event) {
				switch (event.getSlot()) {
				case 1:
					chameleonData.nextArmor(0);
					event.getCurrentItem().setType(chameleonData.getArmor(0));
					// equipmentSpoofer.updateArmor(player, chameleonData, EquipmentSlot.HELMET);
					break;
				case 3:
					chameleonData.nextArmor(1);
					event.getCurrentItem().setType(chameleonData.getArmor(1));
					// equipmentSpoofer.updateArmor(player, chameleonData, EquipmentSlot.CHESTPLATE);
					break;
				case 5:
					chameleonData.nextArmor(2);
					event.getCurrentItem().setType(chameleonData.getArmor(2));
					// equipmentSpoofer.updateArmor(player, chameleonData, EquipmentSlot.LEGGINGS);
					break;
				case 7:
					chameleonData.nextArmor(3);
					event.getCurrentItem().setType(chameleonData.getArmor(3));
					// equipmentSpoofer.updateArmor(player, chameleonData, EquipmentSlot.BOOTS);
					break;
				case 22:
					chameleonData.toggle();
					event.setCurrentItem(PluginUtils.genColoredWool(chameleonData.isEnabled() ? DyeColor.LIME : DyeColor.RED));
					// equipmentSpoofer.updateAllArmor(player, chameleonData);
					break;
				default:
					break;
				}
			}
			
		}.setItem(1, new ItemStack(chameleonData.getArmor(0)))
		.setItem(3, new ItemStack(chameleonData.getArmor(1)))
		.setItem(5, new ItemStack(chameleonData.getArmor(2)))
		.setItem(7, new ItemStack(chameleonData.getArmor(3)))
		.setItem(10, PluginUtils.genColoredWool(DyeColor.LIGHT_BLUE))
		.setItem(12, PluginUtils.genColoredWool(DyeColor.LIGHT_BLUE))
		.setItem(14, PluginUtils.genColoredWool(DyeColor.LIGHT_BLUE))
		.setItem(16, PluginUtils.genColoredWool(DyeColor.LIGHT_BLUE))
		.setItem(22, PluginUtils.genColoredWool(chameleonData.isEnabled() ? DyeColor.LIME : DyeColor.RED));
		return menu;
	}
	
}
