package net.halalaboos.family.classes.chameleon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.halalaboos.family.ChameleonData;
import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.plugin.util.FakeEquipment;

public class ChameleonEquipmentSpoofer extends FakeEquipment {

	private final ChameleonClass chameleon;
	
	private final FamilyFriendlyClasses classes;
	
	private final Map<UUID, List<UUID>> loadedEntities = new HashMap<UUID, List<UUID>>();
	
	public ChameleonEquipmentSpoofer(ChameleonClass chameleon, FamilyFriendlyClasses classes) {
		super(classes);
		this.chameleon = chameleon;
		this.classes = classes;
	}

	@Override
	protected void onEntitySpawn(Player client, LivingEntity visibleEntity) {
		if (visibleEntity instanceof Player) {
			classes.getLogger().info("ENTITYSPAWN: client:" + client.getDisplayName() + ", visibleEntity:" + ((Player) visibleEntity).getDisplayName() + "");
			if (loadedEntities.containsKey(client.getUniqueId())) {
				List<UUID> entityList = loadedEntities.get(client.getUniqueId());
				if (!entityList.contains(visibleEntity.getUniqueId())) {
					entityList.add(visibleEntity.getUniqueId());
					classes.getLogger().info(client.getDisplayName() + " now spawning " + ((Player) visibleEntity).getDisplayName());
				}
			} else {
				List<UUID> entityList = new ArrayList<UUID>();
				entityList.add(visibleEntity.getUniqueId());
				classes.getLogger().info(client.getDisplayName() + " now spawning " + ((Player) visibleEntity).getDisplayName());
				loadedEntities.put(client.getUniqueId(), entityList);
			}
		}
    }
	
	@Override
	protected void onEntityDespawn(Player client, LivingEntity visibleEntity) {
		if (visibleEntity instanceof Player) {
			if (loadedEntities.containsKey(client.getUniqueId())) {
				List<UUID> entityList = loadedEntities.get(client.getUniqueId());
				entityList.remove(visibleEntity.getUniqueId());
				classes.getLogger().info(client.getDisplayName() + " now despawning " + ((Player) visibleEntity).getDisplayName());
			}
		}
	}
	
	@Override
	protected boolean onEquipmentSending(EquipmentSendingEvent equipmentEvent) {
		if (equipmentEvent.getVisibleEntity() instanceof Player) {
			Player player = (Player) equipmentEvent.getVisibleEntity();
			classes.getLogger().info("Armor: " + equipmentEvent.getEquipment() + ", Client: " + equipmentEvent.getClient().getDisplayName() + ", Player: " + player.getDisplayName());
			if (classes.getChameleonDataHandler().hasMetadata(player)) {
				ChameleonData data = classes.getChameleonDataHandler().getMetadata(player);
				if (chameleon.hasClassEnabled(player)) {
					if (data.isEnabled()) {
						if (equipmentEvent.getSlot() != EquipmentSlot.HELD) {
							Material armor = data.getArmor(4 - equipmentEvent.getSlot().getId());
							if (armor != null) {
								equipmentEvent.getEquipment().setType(armor);
							}
						}
					}
				} else {
					if (data.isEnabled()) {
						if (equipmentEvent.getSlot() != EquipmentSlot.HELD) {
							ItemStack armor = equipmentEvent.getSlot().getEquipment(player);
							if (equipmentEvent.getEquipment() != null && equipmentEvent.getEquipment().getType() != Material.AIR && !equipmentEvent.getEquipment().equals(armor)) {
								equipmentEvent.setEquipment(armor);
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	public void updateAllArmor(Player player, ChameleonData data) {
		List<UUID> players = this.loadedEntities.get(player.getUniqueId());
		if (players != null) {
			for (UUID uuid : players) {
				Player otherPlayer = Bukkit.getPlayer(uuid);
				for (int i = 1; i <= 4; i++)
					this.updateSlot(otherPlayer, player, EquipmentSlot.fromId(i));
			}
		}
	}
	
	public void updateArmor(Player player, ChameleonData data, EquipmentSlot slot) {
		List<UUID> players = this.loadedEntities.get(player.getUniqueId());
		if (players != null) {
			for (UUID uuid : players) {
				Player otherPlayer = Bukkit.getPlayer(uuid);
				this.updateSlot(otherPlayer, player, slot);
			}
		}
	}

}
