package net.halalaboos.family.classes;

import java.util.HashMap;
import java.util.Map;

import net.halalaboos.family.GameClass;
import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.tasks.PlayerDamageTask;
import net.halalaboos.family.util.ClassUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

public class AssassinClass extends GameClass implements Listener {
	
	private final Map<PlayerDamageTask, BukkitTask> tasks = new HashMap<PlayerDamageTask, BukkitTask>();
	
	public AssassinClass(FamilyFriendlyClasses classes) {
		super(classes, "Assassin", new Material[] { Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS });
	}

	@Override
	public void enable() {
		Bukkit.getPluginManager().registerEvents(this, classes);
	}
	
	@Override
	public void disable() {
	}

	@Override
	public void onArmourEquip(Player player) {
		player.setMaxHealth(30);
		ClassUtil.addInfinitePotionEffect(PotionEffectType.SPEED, 2, player);
		classes.sendNotificationMessage(player, "You are now an assassin.");
	}

	@Override
	public void onArmourUnEquip(Player player) {
		player.setMaxHealth(20);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.SPEED, player);
	}

	@Override
	public void clearPlayerEffects(Player player) {
		player.setMaxHealth(20);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.SPEED, player);
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			final Player player = (Player) event.getEntity();

			if (event.getDamager() instanceof Arrow) {
				Arrow arrow = (Arrow) event.getDamager();
				if (arrow.getShooter() instanceof Player) {
					Player shooter = (Player) arrow.getShooter();
					if (!shooter.equals(player)) {
						if (this.hasClassEnabled(shooter) && !this.hasClassEnabled(player)) {
							player.setMetadata("archerTag", new FixedMetadataValue(classes, new ArcherTag(1.2F, 6000)));
							classes.sendErrorMessage(shooter, "You've archertagged " + player.getDisplayName());
							classes.sendErrorMessage(player, "You've been archertagged by " + shooter.getDisplayName());
						}
					}
				}
			}
			
			if (player.hasMetadata("archerTag")) {
				ArcherTag tag = (ArcherTag) player.getMetadata("archerTag").get(0).value();
				if (tag.isActive()) {
					event.setDamage(event.getDamage() * tag.getAmount());
				} else {
					player.removeMetadata("archerTag", classes);
					// classes.sendErrorMessage(player, "You are no longer archertagged.");
				}
			}
		}
	}
	
	public void onTaskEnd(PlayerDamageTask damageTask) {
		synchronized (tasks) {
			if (tasks.containsKey(damageTask)) {
				tasks.get(damageTask).cancel();
				tasks.remove(damageTask);
			}
		}
	}
	
}
