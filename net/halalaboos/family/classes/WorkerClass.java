package net.halalaboos.family.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.halalaboos.family.GameClass;
import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.quickplace.BasicQuickPlace;
import net.halalaboos.family.quickplace.QuickPlace;
import net.halalaboos.family.tasks.QuickPlaceTask;
import net.halalaboos.family.util.ClassUtil;
import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class WorkerClass extends GameClass implements Listener {

	private final List<UUID> hasteList = new ArrayList<UUID>();
	
	private final Map<UUID, QuickPlace> quickPlaceMap = new HashMap<UUID, QuickPlace>();

	public WorkerClass(FamilyFriendlyClasses classes) {
		super(classes, "Worker", new Material[] { Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS });
	}

	@Override
	public void enable() {
		Bukkit.getPluginManager().registerEvents(this, classes);
	}

	@Override
	public void disable() {
	}
	
	@Override
	public void onArmourEquip(Player player) {
		ClassUtil.addInfinitePotionEffect(PotionEffectType.NIGHT_VISION, 3, player);
		ClassUtil.addInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, 3, player);
		classes.sendNotificationMessage(player, "You are now a worker.");
	}

	@Override
	public void onArmourUnEquip(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.NIGHT_VISION, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, player);
	}
	

	@Override
	public void clearPlayerEffects(Player player) {
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.NIGHT_VISION, player);
		ClassUtil.removeInfinitePotionEffect(PotionEffectType.FIRE_RESISTANCE, player);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerSwitchItem(PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		if (this.hasClassEnabled(player)) {
			ItemStack oldItem = player.getInventory().getItem(event.getPreviousSlot());
			ItemStack newItem = player.getInventory().getItem(event.getNewSlot());
			if ((oldItem != null && oldItem.getType() == Material.LEASH) && (newItem == null ? true : newItem.getType() != Material.LEASH)) {
				if (hasQuickPlace(player)) {
					QuickPlace quickPlace = quickPlaceMap.get(player.getUniqueId());
					if (quickPlace.isEditing()) {
						quickPlace.cancelEditing(player);
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (this.hasClassEnabled(player)) {
			if (hasQuickPlace(player)) {
				QuickPlace quickPlace = quickPlaceMap.get(player.getUniqueId());
				if (quickPlace.isEditing() && !quickPlace.isWithinEditing(player)) {
					quickPlace.cancelEditing(player);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerUse(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		boolean isSugar = event.getItem() != null && event.getItem().getType() == Material.SUGAR;
		boolean isLeash = event.getItem() != null && event.getItem().getType() == Material.LEASH;
		if (this.hasClassEnabled(player) && player.isSneaking()) {
			if (isSugar) {
				if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
					if (hasCooldown(player)) {
						classes.sendErrorMessage(player, "You must wait to do this.");
					} else {
						int time = event.getItem().getAmount() * 40;
						ClassUtil.increasePotionEffect(PotionEffectType.FAST_DIGGING, time, 1, player);
						player.getInventory().removeItem(event.getItem());
						synchronized (hasteList) {
							hasteList.add(event.getPlayer().getUniqueId());
						}
						classes.sendConfirmMessage(player, "You now have " + ChatColor.RED + PluginUtils.getTime((time / 20) * 1000) + ChatColor.GREEN + " of Haste!");
						Bukkit.getScheduler().scheduleSyncDelayedTask(classes, new Runnable() {
		
							@Override
							public void run() {
								synchronized (hasteList) {
									hasteList.remove(player.getUniqueId());
									classes.sendNotificationMessage(player, "You can now use sugar again.");
								}
							}
							
						}, time + 20 * 120);
					}
				}
			} else if (isLeash) {
				if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (hasQuickPlace(player)) {
						QuickPlace quickPlace = quickPlaceMap.get(player.getUniqueId());
						if (quickPlace.isSet() && quickPlace.canPlace(player, event.getPlayer().getWorld(), player.getItemInHand(), event.getClickedBlock().getRelative(event.getBlockFace()))) {
							Bukkit.getScheduler().scheduleSyncDelayedTask(classes, new QuickPlaceTask(classes, quickPlace, event.getPlayer(), event.getPlayer().getWorld(), player.getItemInHand(), event.getClickedBlock().getRelative(event.getBlockFace())));
						}
					}
				} else if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
					if (hasQuickPlace(player)) {
						QuickPlace quickPlace = quickPlaceMap.get(player.getUniqueId());
						if (quickPlace.isEditing()) {
							quickPlace.setOffset(player, player.getWorld(), event.getItem(), event.getClickedBlock());
							event.setCancelled(true);
						} else {
							quickPlace.beginRearrangement(player, event.getClickedBlock());
							event.setCancelled(true);
						}
					} else {
						QuickPlace quickPlace = new BasicQuickPlace(classes);
						synchronized (quickPlaceMap) {
							quickPlaceMap.put(player.getUniqueId(), quickPlace);
						}
						quickPlace.beginRearrangement(player, event.getClickedBlock());
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	private boolean hasQuickPlace(Player player) {
		synchronized (quickPlaceMap) {
			return quickPlaceMap.containsKey(player.getUniqueId());
		}
	}
	
	private boolean hasCooldown(Player player) {
		synchronized (hasteList) {
			return hasteList.contains(player.getUniqueId());
		}
	}
	
}
