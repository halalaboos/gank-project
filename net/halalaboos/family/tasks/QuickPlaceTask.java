package net.halalaboos.family.tasks;

import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.quickplace.QuickPlace;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class QuickPlaceTask implements Runnable {

	private final FamilyFriendlyClasses classes;
	
	private final QuickPlace quickPlace;
	
	private final Player player;
	
	private final World world;
	
	private final ItemStack item;
	
	private final Block origin;
	
	public QuickPlaceTask(FamilyFriendlyClasses classes, QuickPlace quickPlace, Player player, World world, ItemStack item, Block origin) {
		this.classes = classes;
		this.quickPlace = quickPlace;
		this.player = player;
		this.world = world;
		this.item = item;
		this.origin = origin;
	}

	@Override
	public void run() {
		quickPlace.place(player, world, item, origin);
		// classes.sendConfirmMessage(player, "");
	}

}
