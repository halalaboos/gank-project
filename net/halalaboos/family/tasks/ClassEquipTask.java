package net.halalaboos.family.tasks;

import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.family.GameClass;
import net.halalaboos.family.PlayerData;

import org.bukkit.entity.Player;

public class ClassEquipTask implements Runnable {

	private final FamilyFriendlyClasses classes;
	
	private final Player player;
	
	private final GameClass gameClass;
	
	public ClassEquipTask(FamilyFriendlyClasses classes, Player player, GameClass gameClass) {
		this.classes = classes;
		this.player = player;
		this.gameClass = gameClass;
	}
	
	@Override
	public void run() {
		PlayerData playerData = classes.getPlayerDataHandler().getMetadata(player);
		if (!playerData.isClassEnabled() && gameClass.hasClass(player)) {
			gameClass.onArmourEquip(player);
			playerData.setCurrentClass(gameClass);
			playerData.setClassEnabled(true);
		}
	}

}
