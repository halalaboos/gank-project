package net.halalaboos.family.tasks;

import java.util.Collection;

import net.halalaboos.family.FamilyFriendlyClasses;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;

import pw.gank.factions.api.Faction;
import pw.gank.factions.api.Member;

public class BroadcastPotionTask implements Runnable {

	private final FamilyFriendlyClasses classes;
	
	private final Faction faction;
	
	private final Player player;
	
	private final ItemStack item;
	
	private final PotionMeta metaData;
	
	public BroadcastPotionTask(FamilyFriendlyClasses classes, Faction faction, Player player, ItemStack item, PotionMeta metaData) {
		this.classes = classes;
		this.faction = faction;
		this.player = player;
		this.item = item;
		this.metaData = metaData;
	}
	
	@Override
	public void run() {
		Collection<Member> members = faction.getMembers();
		int memberCount = 0;
		String playerNames = "";
		String potionName = metaData.getDisplayName();
		for (Member member : members) {
			Player factionMember = Bukkit.getPlayer(member.getId());
			if (factionMember != player && factionMember.isOnline()) {
				if (factionMember.getLocation().distance(player.getLocation()) <= 25) {
					factionMember.addPotionEffects(metaData.getCustomEffects());
					classes.sendNotificationMessage(factionMember, player.getName() + " has given you " + potionName + "!");
					playerNames += factionMember.getName() + ", ";
					memberCount++;
				}
			}
		}
		if (memberCount > 0) {
			playerNames = playerNames.substring(0, playerNames.length() - 2);
			classes.sendConfirmMessage(player, String.format("%s recieved %s! (%d)", playerNames, potionName, memberCount));
		}
	}

	/*private void addPotionEffects(Player player, PotionMeta metaData) {
		for (Enchantment enchantment : metaData.getEnchants().keySet()) {
			player.addPotionEffect(enchantment);
		}
	}*/
	
	/*private String getPotionEffects(PotionMeta metaData) {
		String names = "";
		names += metaData.getBasePotionData().getType().getEffectType().getName().toLowerCase().replaceAll("_", " ") + ", ";
		for (PotionEffect effect : metaData.getCustomEffects()) {
			names += effect.getType().getName().toLowerCase().replaceAll("_", " ") + ", ";
		}
		return names.length() > 0 ? names.substring(0, names.length() - 2) : names;
	}*/
	
}
