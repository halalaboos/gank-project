package net.halalaboos.family.tasks;

import net.halalaboos.family.classes.AssassinClass;

import org.bukkit.entity.Player;

public class PlayerDamageTask implements Runnable {

	private final AssassinClass assassinClass;
	
	private final Player player;
	
	private final double damage;
		
	private final int damageCount;
	
	private int count = 0;
	
	public PlayerDamageTask(AssassinClass assassinClass, Player player, double damage, int damageCount) {
		this.assassinClass = assassinClass;
		this.player = player;
		this.damage = damage;
		this.damageCount = damageCount;
	}
	
	@Override
	public void run() {
		player.damage(damage);
		count++;
		if (count >= damageCount) {
			assassinClass.onTaskEnd(this);
		}
	}
	
}
