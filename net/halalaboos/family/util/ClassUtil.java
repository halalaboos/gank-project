package net.halalaboos.family.util;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ClassUtil {

	private static final int MAX_POTION_DURATION =
	/* 20 minutes = seconds */ 1200 * /* ticks conversion */ 20;

	public static void clearInfinitePotionEffects(Player player) {
		for (PotionEffect pe : player.getActivePotionEffects()) {
			/* No potion is longer than 20 minutes */
			if (pe.getDuration() >= MAX_POTION_DURATION) {
				player.removePotionEffect(pe.getType());
			}
		}
	}

	public static void addInfinitePotionEffect(PotionEffectType effectType, int amplifier, Player player) {
		if (player.hasPotionEffect(effectType)) {
			player.removePotionEffect(effectType);
		}
		player.addPotionEffect(new PotionEffect(effectType, Integer.MAX_VALUE, amplifier));
	}

	public static void removeInfinitePotionEffect(PotionEffectType effectType, Player player) {
		for (PotionEffect pe : player.getActivePotionEffects()) {
			/* No potion is longer than 20 minutes */
			if (pe.getDuration() >= MAX_POTION_DURATION && pe.getType().equals(effectType)) {
				player.removePotionEffect(pe.getType());
			}
		}
	}
	
	public static void addPotionEffect(PotionEffectType effectType, int duration, int amplifier, Player player) {
		player.addPotionEffect(new PotionEffect(effectType, duration, amplifier));
	}
	
	public static void increasePotionEffect(PotionEffectType effectType, int duration, int amplifier, Player player) {
		if (player.hasPotionEffect(effectType)) {
			for (PotionEffect pe : player.getActivePotionEffects()) {
				if (effectType == pe.getType())
					player.addPotionEffect(new PotionEffect(pe.getType(), pe.getDuration() + duration, pe.getAmplifier()));
			}
		} else
			player.addPotionEffect(new PotionEffect(effectType, duration, amplifier));
	}

}
