package net.halalaboos.family;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class GameClass {

	protected final FamilyFriendlyClasses classes;
	
	private final String name;
	
	/**
	 * [0] = helmet, [1] = chest-plate, [2] = legs, [3] = boots
	 */
	private Material[] armor;

	public GameClass(FamilyFriendlyClasses classes, String name, Material[] armor) {
		this.classes = classes;
		this.name = name;
		this.armor = armor;
	}
	
	public abstract void enable();
	
	public abstract void disable();

	public abstract void onArmourEquip(Player player);

	public abstract void onArmourUnEquip(Player player);
	
	public abstract void clearPlayerEffects(Player player);

	public boolean hasClassEnabled(Player player) {
		PlayerData playerData = classes.getPlayerDataHandler().getMetadata(player);
		return playerData.isClassEnabled();
	}
	
	public boolean hasClass(Player player) {
		ItemStack[] armorContents = player.getInventory().getArmorContents();
		for (int i = 0; i < armor.length; i++) {
			ItemStack armor = armorContents[3 - i];
			if (armor == null || this.armor[i] != armor.getType())
				return false;
		}
		return true;
	}
	
	public boolean hasClassWithItem(Player player, ItemStack item, int armorIndex) {
		if (!isClassArmor(item))
			return false;
		ItemStack[] armorContents = player.getInventory().getArmorContents();
		for (int i = 0; i < armor.length; i++) {
			if (i == armorIndex)
				continue;
			
			ItemStack armor = armorContents[3 - i];
			if (armor == null || this.armor[i] != armor.getType())
				return false;
		}
		return true;
	}
	
	public boolean isClassArmor(ItemStack item) {
		if (item != null) {
			for (int i = 0; i < armor.length; i++) {
				if (this.armor[i] == item.getType())
					return true;
			}
		}
		return false;
	}
	
	public Material[] getArmor() {
		return armor;
	}

	public void setArmor(Material[] armor) {
		this.armor = armor;
	}

	public String getName() {
		return name;
	}

	public enum EffectType {
		TICK;
	}
}
