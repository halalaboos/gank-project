package net.halalaboos.family.quickplace;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

public class QuickPlaceData {
	
	public int[] format = new int[] {
			0, 0, 0,
			0, 0, 0, 
			0, 0, 0, };
	
	public MaterialData[] materials = new MaterialData[] {
			new MaterialData(Material.AIR),
			new MaterialData(Material.AIR),
			new MaterialData(Material.AIR)
	};
	
	public QuickPlaceData(QuickPlaceData data) {
		this(data.format, data.materials);
	}
	
	public QuickPlaceData(int[] format, MaterialData[] materials) {
		this();
		this.format = format;
		this.materials = materials;
	}
	
	public QuickPlaceData() {
	}
	
	public boolean isMaterial(int index, ItemStack item) {
		return materials[0].equals(item.getData());
	}
	
	public void setOrigin(MaterialData material) {
		setData(0, material, 0, 0, 0);
	}
	
	public void setData(int index, MaterialData material, int xOffset, int yOffset, int zOffset) {
		materials[index] = material;
		format[index * 3] = xOffset;
		format[index * 3 + 1] = yOffset;
		format[index * 3 + 2] = zOffset;
	}
	
	public boolean isSet() {
		for (int i = 0; i < format.length; i++)
			if (format[i] != 0)
				return true;
		for (int i = 0; i < materials.length; i++)
			if (materials[i].getItemType() != Material.AIR)
				return true;
		return false;
	}
	
	public void reset() {
		format = new int[] {
				0, 0, 0,
				0, 0, 0, 
				0, 0, 0, };
		
		materials = new MaterialData[] {
				new MaterialData(Material.AIR),
				new MaterialData(Material.AIR),
				new MaterialData(Material.AIR)
		};
	}
	
	public QuickPlaceData copy() {
		return new QuickPlaceData(this);
	}
	
}
