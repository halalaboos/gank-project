package net.halalaboos.family.quickplace;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface QuickPlace {
	
	boolean place(Player player, World world, ItemStack item, Block origin);
	
	boolean canPlace(Player player, World world, ItemStack item, Block origin);
	
	boolean isSet();
	
	boolean isEditing();
	
	boolean hasAllSet();
	
	boolean isOriginalBlock(ItemStack item);
	
	boolean isWithinEditing(Player player);
	
	void beginRearrangement(Player player, Block origin);
	
	void setOffset(Player player, World world, ItemStack item, Block placement);
	
	void endArrangement();

	void cancelEditing(Player player);
	
}
