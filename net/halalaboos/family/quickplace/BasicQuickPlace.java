package net.halalaboos.family.quickplace;

import java.util.Optional;

import net.halalaboos.family.FamilyFriendlyClasses;
import net.halalaboos.plugin.Default;
import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import pw.gank.factions.api.Faction;
import pw.gank.factions.impl.bukkit.core.FactionsPlugin;

public class BasicQuickPlace implements QuickPlace {

	protected static final int EDIT_TIME = 60000;
	protected static final int EDIT_DISTANCE = 10;

	private final FamilyFriendlyClasses classes;
	
	private long lastEdit = 0L;
	
	private QuickPlaceData oldData = new QuickPlaceData();
	private QuickPlaceData data = new QuickPlaceData();
	
	private int index = 0;
	private boolean editing = false;
	private Block origin = null;
	private Block[] placements = new Block[] { null, null, null };

	public BasicQuickPlace(FamilyFriendlyClasses classes) {
		this.classes = classes;
	}
	
	@Override
	public boolean place(Player player, World world, ItemStack item, Block origin) {
		for (int i = 0; i < 3; i++) {
			int x = origin.getLocation().getBlockX() + data.format[(i * 3)];
			int y = origin.getLocation().getBlockY() + data.format[(i * 3) + 1];
			int z = origin.getLocation().getBlockZ() + data.format[(i * 3) + 2];
			Block block = world.getBlockAt(x, y, z);
			if (block.getType() == Material.AIR && !Default.instance.isWithinProtectedArea(world, x, y, z)) {
				FactionsPlugin factions = Default.instance.getFactions();
				Optional<Faction> factionOptional = factions.getBoard().getClaims().getFactionAt(world.getUID(), x, y, z);
				if (factionOptional.isPresent()) {
					Faction faction = factionOptional.get();
					if (faction.isMember(player.getUniqueId()) || faction.isRaidable()) {
						PluginUtils.placePlayerItem(player, data.materials[i], block);
					}
				} else
					PluginUtils.placePlayerItem(player, data.materials[i], block);
			}
		}
		return true;
	}

	@Override
	public boolean canPlace(Player player, World world, ItemStack item, Block origin) {
		int count = 0;
		MaterialData[] items = { null, null, null };
		int[] slots = { -1, -1, -1 };
		int[] stackSize = { 0, 0, 0 };
		
		for (MaterialData material : data.materials) {
			boolean hasMaterial = false;
			for (int i = 0; i < player.getInventory().getSize(); i++) {
				ItemStack itemStack = player.getInventory().getItem(i);
				if (itemStack != null) {
					if (PluginUtils.compare(material, itemStack.getData())) {
						boolean enoughStack = true;
						for (int j = 0; j < slots.length; j++) {
							if (slots[j] == i && items[j] != null && PluginUtils.compare(items[j], material)) {
								if (stackSize[j] < 1) {
									enoughStack = false;
								} else {
									stackSize[j] = stackSize[j] - 1;
								}
								break;
							}
						}
						
						if (enoughStack) {
							items[count] = itemStack.getData();
							slots[count] = i;
							stackSize[count] = itemStack.getAmount() - 1;
							hasMaterial = true;
							count++;
							break;
						}
					}
				}
			}
			if (!hasMaterial)
				return false;
		}
		return true;
	}

	@Override
	public void beginRearrangement(Player player, Block origin) {
		lastEdit = PluginUtils.getTime();
		if (data.isSet())
			oldData = data.copy();
		data.reset();
		editing = true;
		this.origin = origin;
		data.setOrigin(origin.getState().getData());
		placements = new Block[] { origin, null, null };
		index = 1;
		classes.sendNotificationMessage(player, "Origin set! Select the next two blocks.");
	}

	@Override
	public void setOffset(Player player, World world, ItemStack item, Block placement) {
		if (isEditing()) {
			if (placement.getType().isBlock() && isAdjacent(placement)) {
				lastEdit = PluginUtils.getTime();
				placements[index] = placement;
				int xOffset = placement.getLocation().getBlockX() - origin.getLocation().getBlockX();
				int yOffset = placement.getLocation().getBlockY() - origin.getLocation().getBlockY();
				int zOffset = placement.getLocation().getBlockZ() - origin.getLocation().getBlockZ();
				data.setData(index, placement.getState().getData(), xOffset, yOffset, zOffset);
				index++;
				if (hasAllSet()) {
					endArrangement();
					classes.sendConfirmMessage(player, "Position 2 set! QuickPlace is now ready.");
				} else
					classes.sendNotificationMessage(player, "Position " + (index - 1) + " set!");
			}
		} else {
			this.cancelEditing(player);
		}
	}
	
	private boolean isAdjacent(Block newPlacement) {
		for (BlockFace blockface : BlockFace.values()) {
			for (Block placement : placements) {
				if (placement != null) {
					Block relative = newPlacement.getRelative(blockface);
					if (relative.getLocation().equals(placement.getLocation())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void endArrangement() {
		lastEdit = 0;
		editing = false;
		this.origin = null;
		this.placements = new Block[] { null, null, null };
		oldData.reset();
	}

	@Override
	public boolean isSet() {
		return data.isSet();
	}

	@Override
	public boolean isEditing() {
		if (lastEdit != 0 && (PluginUtils.getTime() - this.lastEdit) >= EDIT_TIME)
			return false;
		else
			return editing;
	}

	@Override
	public boolean hasAllSet() {
		return index >= 3;
	}
	
	@Override
	public boolean isOriginalBlock(ItemStack item) {
		return data.isMaterial(0, item);
	}

	@Override
	public void cancelEditing(Player player) {
		lastEdit = 0;
		editing = false;
		this.origin = null;
		this.placements = new Block[] { null, null, null };
		
		if (oldData.isSet())
			data = oldData.copy();
		oldData.reset();
		
		classes.sendErrorMessage(player, "No longer editing QuickPlace!");
	}

	@Override
	public boolean isWithinEditing(Player player) {
		return origin.getLocation().distance(player.getLocation()) < EDIT_DISTANCE;
	}
	
}
