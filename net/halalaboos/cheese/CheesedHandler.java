package net.halalaboos.cheese;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.util.PlayerMetadataHandler;

public class CheesedHandler extends PlayerMetadataHandler<Boolean> {

	public CheesedHandler(BasePlugin plugin) {
		super(plugin, "cheesed");
		
	}

	@Override
	protected void withoutMetadata(Player player) {
	}

	@Override
	protected void withMetadata(Player player) {
		plugin.sendErrorMessage(player, "You're cheesy!");
	}

	@Override
	public MetadataValue getMetadataValue(Boolean value) {
		return new FixedMetadataValue(plugin, value);
	}

	@Override
	public Boolean getMetadata(Player player) {
		List<MetadataValue> values = player.getMetadata(handleId);
    	return values.get(0).asBoolean();
	}

	@Override
	public boolean applyMetadata(Player player, Boolean value) {
		player.setMetadata(handleId, getMetadataValue(value));
		return false;
	}

}
