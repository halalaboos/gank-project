package net.halalaboos.cheese;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.example.android.apis.view.Cheeses;

import net.halalaboos.cheese.command.DeCheese;
import net.halalaboos.cheese.command.SetCheese;
import net.halalaboos.plugin.BasePlugin;

public class Cheesify extends BasePlugin implements Listener {

	public final Random random = new Random();
	
	public final CheesedHandler cheesedHandler;
	
	public Cheesify() {
		super("Cheesify", "cheese.default", "cheese.admin");
		cheesedHandler = new CheesedHandler(this);
	}

	@Override
	protected void enable() {
		this.getServer().getPluginManager().registerEvents(this, this);
       	this.getCommand("cheese").setExecutor(new SetCheese(this));
       	this.getCommand("decheese").setExecutor(new DeCheese(this));
       	cheesedHandler.load(getConfig());
       	getLogger().info(Cheeses.sCheeseStrings.length + " cheeses loaded.");
	}

	@Override
	protected void disable() {
		cheesedHandler.copyAllMetadata();
		cheesedHandler.save(getConfig());
		this.saveConfig();
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		if (cheesedHandler.hasMetadata(event.getPlayer()) && cheesedHandler.getMetadata(event.getPlayer())) {
			String cheese = Cheeses.findCheese(random);
			event.setMessage(cheese);
			// event.getRecipients().clear();
			// event.getRecipients().add(event.getPlayer());
		}
	}

	public boolean cheese(Player player) {
		if (!cheesedHandler.hasMetadata(player) || (cheesedHandler.hasMetadata(player) && !cheesedHandler.getMetadata(player))) {
			cheesedHandler.applyMetadata(player, true);
			sendErrorMessage(player, "You've been cheesed!");
			return true;
		} else
			return false;
	}

	public boolean decheese(Player player) {
		if (cheesedHandler.hasMetadata(player)) {
			cheesedHandler.removeMetadata(player);
			sendErrorMessage(player, "You're no longer cheesy!");
			return true;
		} else
			return false;
	}

}
