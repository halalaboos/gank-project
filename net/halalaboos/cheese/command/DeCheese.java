package net.halalaboos.cheese.command;

import net.halalaboos.cheese.Cheesify;
import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DeCheese implements CommandExecutor {

	private final Cheesify cheesify;
	
	public DeCheese(Cheesify cheesify) {
		this.cheesify = cheesify;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission(cheesify.getAdminPermissions())) {
			if (args.length > 0) {
				Player player = PluginUtils.getPlayer(args[0]);
				if (player != null) {
					if (cheesify.decheese(player)) {
						cheesify.sendConfirmMessage(sender, String.format("%s is no longer cheesy.", player.getDisplayName()));
					} else
						cheesify.sendErrorMessage(sender, String.format("%s was not cheesy.", player.getDisplayName()));

				}
			} else
				cheesify.sendErrorMessage(sender, "Usage: /decheese <player>");
		}
		return true;
	}


}
