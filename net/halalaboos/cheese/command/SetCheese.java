package net.halalaboos.cheese.command;

import net.halalaboos.cheese.Cheesify;
import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetCheese implements CommandExecutor {

	private final Cheesify cheesify;
	
	public SetCheese(Cheesify cheesify) {
		this.cheesify = cheesify;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission(cheesify.getAdminPermissions())) {
			if (args.length > 0) {
				Player player = PluginUtils.getPlayer(args[0]);
				if (player != null) {
					if (cheesify.cheese(player)) {
						cheesify.sendConfirmMessage(sender, String.format("%s is now cheesy.", player.getDisplayName()));
					} else {
						cheesify.sendErrorMessage(sender, String.format("%s is already cheesy.", player.getDisplayName()));
					}
				}
			} else
				cheesify.sendErrorMessage(sender, "Usage: /cheese <player>");
		}
		return true;
	}


}
