package net.halalaboos.antispam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.settings.Value;

public class AntiSpam extends BasePlugin implements Listener {

	private final Map<UUID, PlayerChatData> playerChatData = new HashMap<UUID, PlayerChatData>();
	
	private Value maxMessages = new Value("Max Messages", "Maximum messages to save to check for spamming", 5D);
	private Value minDistance = new Value("Minimum Distance", "Minimum distance between each word", 10D);

	public AntiSpam() {
		super("AntiSpam", "antispam.default", "antispam.admin");
		this.addSaveable(maxMessages, minDistance);
	}

	@Override
	protected void enable() {
		this.getServer().getPluginManager().registerEvents(this, this);
       	this.createSettingsHandler("antispam");
	}

	@Override
	protected void disable() {
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		UUID uuid = event.getPlayer().getUniqueId();
		String cleanedupMessage = cleanUp(event.getMessage());

		if (playerChatData.containsKey(uuid)) {
			PlayerChatData chatData = playerChatData.get(uuid);
			List<String> messages = chatData.getMessages();
			for (int i = 0; i < messages.size(); i++) {
				String message = messages.get(i);
				int distance = LevenshteinDistance.computeLevenshteinDistance(message, cleanedupMessage);
				int offense = cleanedupMessage.length() - distance;
				if (distance <= minDistance.getValue()) {
					event.setCancelled(true);
					chatData.incrementOffenses(offense);
					this.sendNotificationMessage(event.getPlayer(), String.format("No spamming! (%d distance, %d offenses)", distance, chatData.getOffenses()));
					break;
				}
			}
			chatData.addMessage(cleanedupMessage);
			chatData.trimMessages((int) maxMessages.getValue());
		} else {
			PlayerChatData chatData = new PlayerChatData(event.getPlayer());
			chatData.addMessage(cleanedupMessage);
			playerChatData.put(uuid, chatData);
		}
	}

	private String cleanUp(String message) {
		return message.toLowerCase().replaceAll(" ", "");
	}

}
