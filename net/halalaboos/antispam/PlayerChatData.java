package net.halalaboos.antispam;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

public class PlayerChatData {
	
	private final UUID uuid;
	
	private List<String> messages = new ArrayList<String>();
	
	private int offenses;
		
	public PlayerChatData(Player player) {
		this.uuid = player.getUniqueId();
	}
	
	public void incrementOffenses(int amount) {
		offenses += amount;
	}
	
	public int getOffenses() {
		return offenses;
	}

	public void addMessage(String message) {
		messages.add(0, message);
	}
	
	public void trimMessages(int maxMessages) {
		if (messages.size() >= maxMessages) {
			messages = messages.subList(0, maxMessages);
		}
	}
	
	public List<String> getMessages() {
		return messages;
	}

	public UUID getUuid() {
		return uuid;
	}
}
