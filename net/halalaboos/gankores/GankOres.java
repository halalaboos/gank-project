package net.halalaboos.gankores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.halalaboos.gankores.settings.*;
import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.Default;
import net.halalaboos.plugin.commands.CommandHandler;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public final class GankOres extends BasePlugin implements Listener {
	
	public static final int TOOL_DIAMOND = 4, TOOL_IRON = 3, TOOL_GOLD = 2, TOOL_STONE = 1, TOOL_WOOD = 0;
	
	private GankOre[] ores = null;
	
	private Map<Player, Location> mineLocation = new HashMap<Player, Location>();
	
	private int oreDistance = 10;

	public GankOres() {
		super("GankOres", "gankores.default", "gankores.admin");
	}
	
	@Override
	protected void enable() {
		generateGankOres();
		loadGankOres();
		this.getServer().getPluginManager().registerEvents(this, this);
		CommandHandler settingsHandler = new CommandHandler("GankOres", this);
		settingsHandler.loadCommand(new SetProbability(this));
		settingsHandler.loadCommand(new SetHeight(this));
		settingsHandler.loadCommand(new ListOres(this));
		settingsHandler.loadCommand(new OreInfo(this));
		settingsHandler.loadCommand(new SetOreDistance(this));
       	this.getCommand("gankores").setExecutor(settingsHandler);
    }

	@Override
	protected void disable() {
		saveGankOres();
	}
    
	private void generateGankOres() {
    	ores = new GankOre[] { new GankOre("emerald", Material.EMERALD, Material.EMERALD_ORE, TOOL_IRON),
    			new GankOre("gold", Material.GOLD_ORE, Material.GOLD_ORE, TOOL_IRON),
    			new GankOre("diamond", Material.DIAMOND, Material.DIAMOND_ORE, TOOL_IRON),
    			new GankOre("iron", Material.IRON_ORE, Material.IRON_ORE, TOOL_STONE),
    			new GankOre("coal", Material.COAL, Material.COAL_ORE, TOOL_WOOD),
    			new GankOre("redstone", Material.REDSTONE, Material.REDSTONE_ORE, TOOL_IRON),
    			new GankOre("lapis", Material.INK_SACK, (short) 4, Material.LAPIS_ORE, TOOL_STONE) };
	}
	
	public void loadGankOres() {
    	for (GankOre ore : ores) {
    		ore.setHeight(this.getConfig().getInt(ore.name + ".mineHeight"));
    		ore.setMaxProbability(Float.parseFloat(this.getConfig().getString(ore.name + ".probability")));
    	}
    	oreDistance = this.getConfig().getInt("oreDistance");
	}
    
    public void saveGankOres() {
    	for (GankOre ore : ores) {
    		this.getConfig().set(ore.name + ".mineHeight", ore.getHeight());
    		this.getConfig().set(ore.name + ".probability", ore.getMaxProbability());
    	}
    	this.getConfig().set("oreDistance", oreDistance);
    	this.saveConfig();
	}
    
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
    	if (!event.getPlayer().hasPermission(this.getDefaultPermission()) || event.getPlayer().getGameMode().equals(GameMode.CREATIVE) || Default.instance.isTrue(event.getBlock())) {
    		return;
    	}
    	BlockState blockState = event.getBlock().getState();
        Location location = blockState.getLocation().add(0.5F, 0.5F, 0.5F);
        Player player = event.getPlayer();
        Location lastLocation = mineLocation.get(player);
    	if (lastLocation != null && lastLocation.getWorld().equals(location.getWorld()))
    		if (lastLocation.distance(location) < oreDistance)
    			return;
        if (blockState.getType() == Material.STONE) {
            GankOre dropOre = null;
        	int playerItem = getItem(player.getItemInHand());
        	float random = (float) Math.random();
        	int fortuneLevel = getFortune(player.getItemInHand());
        	if (fortuneLevel <= 0)
        		fortuneLevel = 1;
        	for (int i = 0; i < ores.length; i++) {
        		GankOre ore = ores[i];
        		if (random < (ore.getProbability(location) * fortuneLevel) && playerItem >= ore.toolPreference) {
                   	if (dropOre == null)
                   		dropOre = ore;
                   	else
                   		if (ore.getMaxProbability() < dropOre.getMaxProbability())
                   			dropOre = ore;
        		}
        	}
        	if (dropOre != null) {
            	int silkTouchLevel = getSilkTouch(player.getItemInHand());
        		ItemStack droppedItem = silkTouchLevel >= 1 ? dropOre.oreItem : dropOre.minedItem;
        		event.setCancelled(true);
        		event.getBlock().setType(Material.AIR);
        		location.getWorld().dropItemNaturally(location, droppedItem);
        		mineLocation.put(player, location);
        		// player.getLocation().getWorld().playEffect(player.getLocation(), Effect., 1);
        	}
        }
    }
    
    private int getSilkTouch(ItemStack item) {
    	if (item != null) {
    		return item.getEnchantmentLevel(Enchantment.SILK_TOUCH);
    	} else {
    		return 0;
    	}
    }
    
    private int getFortune(ItemStack item) {
    	if (item != null) {
    		return 1 + item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
    	} else {
    		return 1;
    	}
    }
    
    private int getItem(ItemStack item) {
    	if (item != null) {
	    	switch (item.getType()) {
	    		case DIAMOND_PICKAXE:
	    			return TOOL_DIAMOND;
	    		case IRON_PICKAXE:
	    			return TOOL_IRON;
	    		case GOLD_PICKAXE:
	    			return TOOL_GOLD;
	    		case STONE_PICKAXE:
	    			return TOOL_STONE;
	    		case WOOD_PICKAXE:
	    			return TOOL_WOOD;
	    		default:
	    			return -1;
	    	}
    	} else {
    		return -1;
    	}
    }

	public GankOre[] getOres() {
		return ores;
	}

	public void setOres(GankOre[] ores) {
		this.ores = ores;
	}

	public int getOreDistance() {
		return oreDistance;
	}

	public void setOreDistance(int oreDistance) {
		this.oreDistance = oreDistance;
	}

	public Map<Player, Location> getMineLocation() {
		return mineLocation;
	}
}
