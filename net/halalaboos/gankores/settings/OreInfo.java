package net.halalaboos.gankores.settings;

import org.bukkit.command.CommandSender;
import net.halalaboos.gankores.GankOre;
import net.halalaboos.gankores.GankOres;
import net.halalaboos.plugin.commands.InternalCommand;

public class OreInfo implements InternalCommand {

	private final GankOres gankOres;
	
	public OreInfo(GankOres gankOres) {
		this.gankOres = gankOres;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "info", "i" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Ore drop information.";
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 2) {
    		for (int i = 0; i < gankOres.getOres().length; i++) {
		    	GankOre ore = gankOres.getOres()[i];
		    	if (ore.name.startsWith(args[1].toLowerCase())) {
		    		sender.sendMessage(ore.toString());
		    		return;
		    	}
    		}
		    gankOres.sendNotificationMessage(sender, "'" + args[1] + "' is not a registered ore. Type '/gankores list' for a full list of ores.");
		} else
			gankOres.sendNotificationMessage(sender, "Usage: /gankores info <ore>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(gankOres.getAdminPermissions());
	}

}
