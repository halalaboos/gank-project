package net.halalaboos.gankores.settings;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import net.halalaboos.gankores.GankOre;
import net.halalaboos.gankores.GankOres;
import net.halalaboos.plugin.commands.InternalCommand;

public class ListOres implements InternalCommand {

	private final GankOres gankOres;
	
	public ListOres(GankOres gankOres) {
		this.gankOres = gankOres;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "list", "l" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "List of ores.";
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		String oreInformation = "Ores: ";
		for (int i = 0; i < gankOres.getOres().length; i++) {
	    	GankOre ore = gankOres.getOres()[i];
	    	oreInformation += ChatColor.YELLOW + ore.name + ChatColor.WHITE + ", ";
		}
		gankOres.sendNotificationMessage(sender, oreInformation.substring(0, oreInformation.length() - 2));
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(gankOres.getAdminPermissions());
	}

}
