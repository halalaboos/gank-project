package net.halalaboos.gankores.settings;

import org.bukkit.command.CommandSender;
import net.halalaboos.gankores.GankOres;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;

public class SetOreDistance implements InternalCommand {

	private final GankOres gankOres;
	
	public SetOreDistance(GankOres gankOres) {
		this.gankOres = gankOres;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "oredistance", "o" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Set distance between each mined ore.";
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 2) {
			if (PluginUtils.isInteger(args[1])) {
				gankOres.setOreDistance(Integer.parseInt(args[1]));
				gankOres.sendConfirmMessage(sender, "Ore distance is now '" + args[1] + "'.");
			} else
				gankOres.sendErrorMessage(sender, "'" + args[1] + "' is not a valid integer!");
		} else
			gankOres.sendNotificationMessage(sender, "Usage: /gankores oredistance <value>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(gankOres.getAdminPermissions());
	}

}
