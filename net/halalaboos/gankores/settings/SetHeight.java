package net.halalaboos.gankores.settings;

import org.bukkit.command.CommandSender;

import net.halalaboos.gankores.GankOre;
import net.halalaboos.gankores.GankOres;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;

public class SetHeight implements InternalCommand {

	private final GankOres gankOres;
	
	public SetHeight(GankOres gankOres) {
		this.gankOres = gankOres;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "height", "h" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Set minimum height to drop ore.";
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 3) {
			if (!PluginUtils.isInteger(args[2])) {
				gankOres.sendErrorMessage(sender, "'" + args[2] + "' is not a valid integer!");
			}
		    for (int i = 0; i < gankOres.getOres().length; i++) {
		    	GankOre ore = gankOres.getOres()[i];
		    	if (ore.name.startsWith(args[1].toLowerCase())) {
			    	int height = Integer.parseInt(args[2]);
			    	ore.setHeight(height);
			    	gankOres.sendConfirmMessage(sender, ore.name + " mine height is now set to " + height + ".");
			    	gankOres.saveGankOres();
			    	return;
		    	}
		    }
		    gankOres.sendErrorMessage(sender, "'" + args[1] + "' is not a registered ore. Type '/gankores list' for a full list of ores.");
		} else
			gankOres.sendNotificationMessage(sender, "Usage: /gankores height <ore>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(gankOres.getAdminPermissions());
	}

}
