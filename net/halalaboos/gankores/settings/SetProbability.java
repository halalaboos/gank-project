package net.halalaboos.gankores.settings;

import org.bukkit.command.CommandSender;

import net.halalaboos.gankores.GankOre;
import net.halalaboos.gankores.GankOres;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.util.PluginUtils;

public class SetProbability implements InternalCommand {

	private final GankOres gankOres;
	
	public SetProbability(GankOres gankOres) {
		this.gankOres = gankOres;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { "probability", "p" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "" };
	}

	@Override
	public String getDescription() {
		return "Set ore drop probability.";
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (args.length >= 3) {
			if (PluginUtils.isFloat(args[2])) {
				 for (int i = 0; i < gankOres.getOres().length; i++) {
				    GankOre ore = gankOres.getOres()[i];
				    if (ore.name.startsWith(args[1].toLowerCase())) {
					   	float probability = Float.parseFloat(args[2]);
					   	ore.setMaxProbability(probability);
					   	gankOres.sendConfirmMessage(sender, ore.name + " mine probability is now set to " + probability + ".");
					   	gankOres.saveGankOres();
					   	return;
				    }
				 }
				 gankOres.sendErrorMessage(sender, "'" + args[1] + "' is not a registered ore. Type '/gankores list' for a full list of ores.");
			} else
				gankOres.sendErrorMessage(sender, "'" + args[2] + "' is not a valid float!");
		} else
			gankOres.sendNotificationMessage(sender, "Usage: /gankores probability <ore> <value>");
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(gankOres.getAdminPermissions());
	}

}
