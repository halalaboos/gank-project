package net.halalaboos.gankores;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class GankOre {

	public final String name;
	
	public final ItemStack oreItem, minedItem;
	
	public final int toolPreference;
	
	private int height;
	
	private float maxProbability;
	
	public GankOre(String name, Material material, Material oreMaterial, int toolPreference) {
		this(name, material, (short) -1, oreMaterial, toolPreference);
	}
	
	public GankOre(String name, Material material, short durability, Material oreMaterial, int toolPreference) {
		this.name = name;
		this.minedItem = new ItemStack(material, 1);
		if (durability != -1) {
			minedItem.setDurability(durability);
		}
		this.oreItem = new ItemStack(oreMaterial, 1);
		this.toolPreference = toolPreference;
	}

	public float getProbability(Location location) {
		return location.getY() <= height ? maxProbability : 0F;
	}
	
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getMaxProbability() {
		return maxProbability;
	}

	public void setMaxProbability(float maxProbability) {
		this.maxProbability = maxProbability;
	}
	
	@Override
	public String toString() {
		return name + ": probability= " + maxProbability + ", height=" + height;
	}
}
