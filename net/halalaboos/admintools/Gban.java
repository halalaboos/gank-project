package net.halalaboos.admintools;

import net.halalaboos.plugin.util.PluginUtils;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gban implements CommandExecutor {

	private final AdminTools admintools;
	
	public Gban(AdminTools admintools) {
		this.admintools = admintools;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission("admintools.admin")) {
			if (args.length > 0) {
				Player player = PluginUtils.getPlayer(args[0]);
				int duration = 
				String reason = args.length >= 2 ? PluginUtils.getAfter(label, 3) : "You have been banned!";
				if (player != null) {
					String address = player.getAddress().getHostName();
					player.setBanned(true);
					player.kickPlayer("");
					Bukkit.getServer().getBanList(Type.IP).addBan(address, reason, new Date, sender.getName());
					Bukkit.getServer().banIP(address);
				} else
					admintools.sendErrorMessage(sender,  String.format("%s could not be found.", args[0]));
			} else
				admintools.sendErrorMessage(sender, "Usage: /gban <player> <duration> <reason>");
		}
		return true;
	}


}
