package net.halalaboos.admintools;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.settings.SimpleFileConfig;

public class AdminTools extends BasePlugin implements Listener {

	private List<String> bannedCommands = new ArrayList<String>();
	
	private Map<String, PlayerData> playerData = new HashMap<String, PlayerData>();
	
	private SimpleFileConfig playerDataConfig;

	public AdminTools() {
		super("AdminTools", "admintools.default", "admintools.admin");
		
	}

	@Override
	protected void enable() {
		this.getServer().getPluginManager().registerEvents(this, this);
		this.bannedCommands = getConfig().getStringList("banned");
		playerDataConfig = new SimpleFileConfig(this, new File(getDataFolder(), "players.yml"));
		playerDataConfig.load();
	}

	@Override
	protected void disable() {
	}
	
	// @EventHandler(priority = EventPriority.HIGHEST)
	// public void onTabComplete(PlayerChatTabCompleteEvent event) {
		
	// }
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (hasPlayerData(event.getHostname())) {
			PlayerData playerData = getPlayerData(event.getHostname());			
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onPreprocess(PlayerCommandPreprocessEvent event) {
    	for (String command : bannedCommands) {
    		if (event.getMessage().startsWith(command)) {
    			event.setCancelled(true);
        	}
    	}
    }
	
	public boolean hasPlayerData(String address) {
		return playerData.containsKey(address);
	}
	
	public PlayerData getPlayerData(String address) {
		return playerData.get(address);
	}

}
