package net.halalaboos.plugin.util;

import net.halalaboos.plugin.BasePlugin;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class InventoryMenu implements Listener {

	protected final BasePlugin plugin;
	
	protected final Inventory inventory;
	
	public InventoryMenu(BasePlugin plugin, String title, int size) {
		this.plugin = plugin;
		inventory = Bukkit.createInventory(null, size, title);
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	public InventoryMenu setItem(int slot, ItemStack item) {
		inventory.setItem(slot, item);
		return this;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			Inventory inventory = event.getInventory();
			if (inventory.getName().equals(this.inventory.getName())) {
				onClickItem(player, event);
				event.setCancelled(true);
			}
		}
	}
	
	public void open(Player player) {
		player.openInventory(inventory);
	}
	
	protected abstract void onClickItem(Player player, InventoryClickEvent event);
	
}
