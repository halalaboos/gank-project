package net.halalaboos.plugin.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.halalaboos.plugin.BasePlugin;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.MetadataValue;

public abstract class PlayerMetadataHandler <V> implements Listener {

	protected final BasePlugin plugin;
	
	protected final String handleId;
	
	private final Map<String, V> metadataMap = new HashMap<String, V>();
	
	public PlayerMetadataHandler(BasePlugin plugin, String handleId) {
		this.plugin = plugin;
		this.handleId = handleId;
	}
	
	protected abstract void withoutMetadata(Player player);

	protected abstract void withMetadata(Player player);

	public abstract MetadataValue getMetadataValue(V value);
	
	public abstract V getMetadata(Player player);
	
	public abstract boolean applyMetadata(Player player, V value);
	
	public void removeMetadata(Player player) {
		player.removeMetadata(handleId, plugin);
	}
	
	public void save(FileConfiguration config) {
		config.createSection(handleId, metadataMap);	
	}
	
	@SuppressWarnings("unchecked")
	public void load(FileConfiguration config) {
		Map<String, Object> loaded = config.getConfigurationSection(handleId).getValues(false);
		if (loaded != null) {
			Set<String> keys = loaded.keySet();
			for (String uuid : keys) {
				V value = (V) loaded.get(uuid);
				metadataMap.put(uuid, value);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
		if (metadataMap.containsKey(event.getPlayer().getUniqueId().toString())) {
			V value = metadataMap.get(event.getPlayer().getUniqueId().toString());
			applyMetadata(event.getPlayer(), value);
			withMetadata(event.getPlayer());
		} else {
			withoutMetadata(event.getPlayer());
		}
    }
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
		if (hasMetadata(event.getPlayer())) {
			this.metadataMap.put(event.getPlayer().getUniqueId().toString(), getMetadata(event.getPlayer()));
		}
    }
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerKick(PlayerKickEvent event) {
		if (hasMetadata(event.getPlayer())) {
			this.metadataMap.put(event.getPlayer().getUniqueId().toString(), getMetadata(event.getPlayer()));
		}
    }
	
	public boolean hasMetadata(Player player) {
    	return player.hasMetadata(handleId);
    }
	
	public void copyAllMetadata() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (hasMetadata(player)) {
				this.metadataMap.put(player.getUniqueId().toString(), getMetadata(player));
			}
		}
	}
	
	public V getMetadata(String uuid) {
		return metadataMap.get(uuid);
	}
	
	public V getMetadata(UUID uuid) {
		return metadataMap.get(uuid.toString());
	}
	
	public boolean hasMetadata(String uuid) {
		return metadataMap.containsKey(uuid);
	}
	
	public boolean hasMetadata(UUID uuid) {
		return metadataMap.containsKey(uuid.toString());
	}
	
	public void putMetadata(String uuid, V data) {
		this.metadataMap.put(uuid, data);
	}
	
	public void putMetadata(UUID uuid, V data) {
		this.metadataMap.put(uuid.toString(), data);
	}
	
}
