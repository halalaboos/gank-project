package net.halalaboos.plugin.util;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class Position implements ConfigurationSerializable {

	private double x, y, z;
	
	private float yaw, pitch;
	
	private String worldName = "";
	
	private World world;
	
	public Position(Map<String, Object> map) {
		this((double) map.get("x"), (double) map.get("y"), (double) map.get("z"), (double) map.get("yaw"), (double) map.get("pitch"), (String) map.get("world"));
	}
	
	public Position(double x, double y, double z, double yaw, double pitch, String world) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = (float) yaw;
		this.pitch = (float) pitch;
		this.worldName = world;
	}
	
	@Override
	public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("x", x);
        map.put("y", y);
        map.put("z", z);
        map.put("yaw", yaw);
        map.put("pitch", pitch);
        map.put("world", worldName);
		return map;
	}
	
	public static Position deserialize(Map<String, Object> map) {
		return new Position((double) map.get("x"), (double) map.get("y"), (double) map.get("z"), (double) map.get("yaw"), (double) map.get("pitch"), (String) map.get("world"));
	}
	
	public static Position valueOf(Map<String, Object> map) {
		return new Position((double) map.get("x"), (double) map.get("y"), (double) map.get("z"), (double) map.get("yaw"), (double) map.get("pitch"), (String) map.get("world"));
	}

	public Location toLocation() {
		return new Location(world, x, y, z, yaw, pitch);
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public World getWorld() {
		if (world == null) {
			world = Bukkit.getWorld(worldName); // DOES NOT WORK?
		}
		return world;
	}

	public void setWorld(World world) {
		this.worldName = world.getName();
		this.world = world;
	}

	public void updateWorld(World world) {
		if (world.getName().equalsIgnoreCase(worldName)) {
			this.world = world;
		}
	}

}
