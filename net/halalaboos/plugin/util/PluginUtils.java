package net.halalaboos.plugin.util;

import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;
import org.bukkit.potion.PotionEffect;

public final class PluginUtils {

	private PluginUtils() {
		
	}
	
	public static ItemStack genColoredWool(DyeColor color) {
		Wool wool = new Wool(color);
		return wool.toItemStack(1);
	}
	
	public static void placePlayerItem(Player player, MaterialData material, Block block) {
		for (int i = 0; i < player.getInventory().getSize(); i++) {
			ItemStack item = player.getInventory().getItem(i);
			if (item != null) { // && item.getType() != Material.AIR
				if (compare(material, item.getData()) && checkEntities(block)) {
					block.setType(material.getItemType());
					block.setData(material.getData());
					if (item.getAmount() <= 1)
						player.getInventory().setItem(i, null);
					else
						item.setAmount(item.getAmount() - 1);
					return;
				}
			}
		}
	}
	
	private static boolean checkEntities(Block block) {
		Entity[] entities = block.getChunk().getEntities();
		for (Entity entity : entities) {
			if (entity.getLocation().distance(block.getLocation()) <= 2) {
				return false;
			}
		}
		return true;
	}

	public static boolean compare(MaterialData materialData, MaterialData item) {
		boolean equalMaterial = materialData.getItemType() == item.getItemType();
		if (equalMaterial) {
			switch (materialData.getItemType()) {
			case LOG_2:
			case LOG:
				byte logData = (byte) ((int) materialData.getData() % 4);
				byte itemData = item.getData();
				return itemData == logData;
			default:
				return item.getData() == materialData.getData();
			}
		} else
			return false;
	}
	
	public static void dropInventory(Player player, World world, Location location) {
		PlayerInventory inventory = player.getInventory();
		for (ItemStack item : inventory.getContents()) {
			if (item != null && item.getType() != Material.AIR)
				world.dropItemNaturally(location, item);
		}
		inventory.clear();
	}
	
	public static void dropArmor(Player player, World world, Location location) {
		ItemStack[] inventory = player.getInventory().getArmorContents();
		for (ItemStack item : inventory) {
			if (item != null && item.getType() != Material.AIR)
				world.dropItemNaturally(location, item);
		}
		player.getInventory().setArmorContents(new ItemStack[4]);
	}
	
	public static void removeAllPotionEffects(Player player) {
		for (PotionEffect effect : player.getActivePotionEffects()) 
			player.removePotionEffect(effect.getType());
	}
	
	public static Player getPlayer(String name) {
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			if (player.getName().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}
	
	/**
	 * Gives you everything after 'x' words in a sentence.
	 * <br>
	 * EX: getEverythingAfter("this is an interesting sentence", 2)
	 * <br>
	 * Returns 'an interesting sentence'
	 * */
	public static String getAfter(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(splitIndex);
	}
	
	public static String getBefore(String text, int index) {
		String[] words = text.split(" ");
		if (words.length < index)
			return null;
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
			splitIndex += words[i].length() + 1;
		return text.substring(0, splitIndex);
	}
	
	public static String replaceIgnoreCase(String input, String regex, String replacement) {
        return input.replaceAll("(?i)" + regex, replacement);
    }
	
	public static String getTime(long time) {
		if (time >= 3600000) {
			long diffHour = time % 3600000;
			return (time / 3600000) + " hours " + (diffHour / 60000) + " minutes";
		} else if (time >= 60000) {
			long diffMinutes = time % 60000;
			return (time / 60000) + " minutes " + (diffMinutes / 1000) + " seconds";
		} else if (time >= 1000) {
			return (time / 1000) + " seconds";
		} else
			return time + " milliseconds";
	}
	
	public static String fixedCommandName(String text) {
		return text.toLowerCase().replaceAll(" ", "_");
	}
	
	/**
	 * @return true if the text could be parsed as an integer.
	 * */
	public static boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public static boolean isDouble(String text) {
		try {
			Double.parseDouble(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * @return true if the text could be parsed as a double.
	 * */
	public static boolean isFloat(String text) {
		try {
			Float.parseFloat(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static long getTime() {
		return new Date().getTime();
	}
	
}
