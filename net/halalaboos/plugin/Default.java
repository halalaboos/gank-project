package net.halalaboos.plugin;

import java.io.File;
import java.util.List;

import net.halalaboos.plugin.util.Position;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import pw.gank.factions.impl.bukkit.core.FactionsPlugin;

import com.gmail.nossr50.util.blockmeta.chunkmeta.ChunkManager;
import com.gmail.nossr50.util.blockmeta.chunkmeta.ChunkManagerFactory;
import com.gmail.nossr50.util.blockmeta.conversion.BlockStoreConversionMain;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Default extends JavaPlugin implements Listener {
	
	public static Default instance;
	
	private int conversionRate = 1;
	
	private ChunkManager placeStore;
	
	private FactionsPlugin factions;
	
	private WorldGuardPlugin worldGuard;
	
	@Override
    public void onEnable() {
       	ConfigurationSerialization.registerClass(Position.class);
		instance = this;
		factions = (FactionsPlugin) Bukkit.getPluginManager().getPlugin("Factions");
		worldGuard = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
		this.placeStore = ChunkManagerFactory.getChunkManager();
		this.getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {
		placeStore.saveAll();
		placeStore.cleanUp();
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent event) {
		BlockState blockState = event.getBlock().getState();
		placeStore.setTrue(blockState);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onStructureGrow(StructureGrowEvent event) {
		if (!placeStore.isTrue(event.getLocation().getBlock())) {
			return;
		}

		for (BlockState blockState : event.getBlocks()) {
			placeStore.setFalse(blockState);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onWorldInit(WorldInitEvent event) {
		World world = event.getWorld();

		if (!new File(world.getWorldFolder(), "gank_data").exists()) {
			return;
		}

		getLogger().info("Converting block storage for " + world.getName() + " to a new format.");

		new BlockStoreConversionMain(world).run();
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onWorldUnload(WorldUnloadEvent event) {
		placeStore.unloadWorld(event.getWorld());
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onChunkUnload(ChunkUnloadEvent event) {
		Chunk chunk = event.getChunk();
		placeStore.chunkUnloaded(chunk.getX(), chunk.getZ(), event.getWorld());
	}

	public int getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(int conversionRate) {
		this.conversionRate = conversionRate;
	}

	public ChunkManager getPlaceStore() {
		return placeStore;
	}

	public boolean isTrue(int x, int y, int z, World world) {
		return placeStore.isTrue(x, y, z, world);
	}

	public boolean isTrue(Block block) {
		return placeStore.isTrue(block);
	}

	public boolean isTrue(BlockState blockState) {
		return placeStore.isTrue(blockState);
	}

	public FactionsPlugin getFactions() {
		return factions;
	}

	public WorldGuardPlugin getWorldGuard() {
		return worldGuard;
	}

	public boolean isWithinProtectedArea(World world, int x, int y, int z) {
		RegionManager manager = worldGuard.getRegionManager(world);
		for (ProtectedRegion region : manager.getRegions().values()) {
			if (region.contains(x, y, z)) {
				return true;
			}
		}
		return false;
	}
}
