package net.halalaboos.plugin;

import java.io.File;
import java.util.List;

import net.halalaboos.plugin.commands.CommandHandler;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.commands.saveable.SaveableCommandFactory;
import net.halalaboos.plugin.settings.Saveable;
import net.halalaboos.plugin.settings.SimpleFileConfig;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class BasePlugin extends JavaPlugin {

	protected final String pluginName, defaultPermissions, adminPermissions;
	
	protected final SimpleFileConfig fileConfig;
	
	private ChatColor notificationFormat = ChatColor.AQUA, confirmFormat = ChatColor.GREEN, errorFormat = ChatColor.RED;
	
	public BasePlugin(String pluginName, String defaultPermissions, String adminPermissions) {
		this.pluginName = pluginName;
		this.defaultPermissions = defaultPermissions;
		this.adminPermissions = adminPermissions;
		fileConfig = new SimpleFileConfig(this, new File(getDataFolder(), "settings.yml"));
	}
	
	@Override
    public void onEnable() {
		enable();
		fileConfig.load();
	}

	@Override
    public void onDisable() {
		disable();
		fileConfig.save();
	}
	
	protected abstract void enable();
	
	protected abstract void disable();
	
	public void sendErrorMessage(CommandSender sender, String[] messages) {
		sendMessage(sender, errorFormat, messages);
	}

	public void sendErrorMessage(CommandSender sender, String message) {
		sendMessage(sender, errorFormat, message);
	}
	
	public void sendConfirmMessage(CommandSender sender, String[] messages) {
		sendMessage(sender, confirmFormat, messages);
	}
	
	public void sendConfirmMessage(CommandSender sender, String message) {
		sendMessage(sender, confirmFormat, message);
	}
	
	public void sendNotificationMessage(CommandSender sender, String[] messages) {
		sendMessage(sender, notificationFormat, messages);
	}
	
	public void sendNotificationMessage(CommandSender sender, String message) {
		sendMessage(sender, notificationFormat, message);
	}
	
	public void sendMessage(CommandSender sender, ChatColor titleColor, String[] messages) {
		for (int i = 0; i < messages.length; i++)
			messages[i] = titleColor + messages[i];
		sender.sendMessage(messages);
	}
	
	public void sendMessage(CommandSender sender, ChatColor titleColor, String message) {
		sender.sendMessage(titleColor + message);
	}

	public ChatColor getNameFormat() {
		return notificationFormat;
	}

	public void setNameFormat(ChatColor nameFormat) {
		this.notificationFormat = nameFormat;
	}
	
	public void addSaveable(Saveable... saveables) {
		this.fileConfig.addSaveable(saveables);
	}
	
	public String getDefaultPermission() {
		return defaultPermissions;
	}
	
	public String getAdminPermissions() {
		return adminPermissions;
	}
	
	public CommandHandler createSettingsHandler(String commandName) {
		CommandHandler commandHandler = new CommandHandler(commandName, this);
       	for (Saveable saveable : fileConfig.getSaveables()) {
       		InternalCommand command = SaveableCommandFactory.getCommand(this, saveable);
       		if (command != null)
       			commandHandler.loadCommand(command);
       	}
       	this.getCommand(commandName).setExecutor(commandHandler);
       	return commandHandler;
	}
}
