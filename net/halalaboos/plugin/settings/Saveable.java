package net.halalaboos.plugin.settings;

import org.bukkit.configuration.file.FileConfiguration;

public interface Saveable {

	void save(FileConfiguration config);
	
	void load(FileConfiguration config);
	
	String getName();
	
	String getDescription();
}
