package net.halalaboos.plugin.settings;

import org.bukkit.configuration.file.FileConfiguration;

public class Toggleable implements Saveable {

	private String name, description;
	
	private boolean enabled = false;
	
	public Toggleable(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	@Override
	public void save(FileConfiguration config) {
		config.set(name, enabled);
	}

	@Override
	public void load(FileConfiguration config) {
		enabled = config.getBoolean(name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
