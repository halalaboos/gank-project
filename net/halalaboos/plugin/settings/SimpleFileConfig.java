package net.halalaboos.plugin.settings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.halalaboos.plugin.BasePlugin;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class SimpleFileConfig {

	private final BasePlugin plugin;
	
	private File file;
	
	private FileConfiguration fileConfiguration;
	
	private List<Saveable> saveables = new ArrayList<Saveable>();
	
	public SimpleFileConfig(BasePlugin plugin, File file) {
		this.plugin = plugin;
		this.file = file;
	}
	
	public void load() {
		this.fileConfiguration = YamlConfiguration.loadConfiguration(file);
		if (file.exists()) {
			for (Saveable saveable : saveables) {
				saveable.load(fileConfiguration);
			}
		}
	}
	
	public void save() {
		try {
			for (Saveable saveable : saveables) {
				saveable.save(fileConfiguration);
			}
			this.fileConfiguration.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		return fileConfiguration.toString();
	}

	public FileConfiguration getFileConfiguration() {
		return fileConfiguration;
	}
	
	public List<Saveable> getSaveables() {
		return saveables;
	}
	
	public void addSaveable(Saveable... saveables) {
		this.saveables.addAll(Arrays.asList(saveables));
	}
}
