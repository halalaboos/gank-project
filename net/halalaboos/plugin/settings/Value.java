package net.halalaboos.plugin.settings;

import org.bukkit.configuration.file.FileConfiguration;

public class Value implements Saveable {
	
	private final double defaultValue;
	
	private String name, description;
		
	private double value = 0F;
	
	public Value(String name, String description, double defaultValue) {
		this.name = name;
		this.description = description;
		this.defaultValue = defaultValue;
		this.value = defaultValue;
	}
	
	@Override
	public void save(FileConfiguration config) {
		config.set(name, value);
	}

	@Override
	public void load(FileConfiguration config) {
		value = config.getDouble(name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getDefaultValue() {
		return defaultValue;
	}
	
	public boolean isDefault() {
		return value == defaultValue;
	}

}
