package net.halalaboos.plugin.commands;

import org.bukkit.command.CommandSender;

public interface InternalCommand {
    
	enum Use {
		PLAYER, CONSOLE, BOTH;
	}
	
	/**
     * @return An array of aliases available for the command.
     */
    public String[] getAliases();

    /**
     * @return An array of helpful information about the command.
     */
    public String[] getHelp();

    /**
     * @return Description of the command.
     */
    public String getDescription();
    
    /**
     * @return Which sender can use the command.
     * */
    public Use getUse();

    /**
     * Handles processing the commands.
     */
    public void run(CommandSender sender, String label, String[] args);
    
    
    /**
     * @return Whether or not the sender has permission.
     * */
    public boolean hasPermission(CommandSender sender);

}