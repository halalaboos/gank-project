package net.halalaboos.plugin.commands;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.halalaboos.plugin.BasePlugin;

public class CommandHandler implements CommandExecutor {
	
	protected final String commandName;
	
	protected final BasePlugin plugin;

	private final List<InternalCommand> commands = new ArrayList<InternalCommand>();

	public CommandHandler(String commandName, BasePlugin plugin) {
		this.commandName = commandName;
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			listCommands(sender);
			return true;
		}
		for (InternalCommand command0 : commands) {
			if (command0.hasPermission(sender)) {
				for (String alias : command0.getAliases()) {
					if (alias.toLowerCase().equals(args[0])) {
						tryCommand(command0, sender, label, args);
						return true;
					}
				}
			}
		}
		plugin.sendErrorMessage(sender, "'" + args[0] + "' is not recognized as a command. Try /" + commandName.toLowerCase() + " for more help.");
		return true;
	}

	private void listCommands(CommandSender sender) {
		List<String> help = new ArrayList<String>();
		help.add(String.format("------------- %s Help -------------", commandName));
		for (int i = 1; i < commands.size() + 1; i++) {
			InternalCommand command = commands.get(i - 1);
			if (command.hasPermission(sender)) {
				help.add(String.format("/%s %s - %s", commandName.toLowerCase(), command.getAliases()[0], command.getDescription()));
			}
		}
		plugin.sendNotificationMessage(sender, help.toArray(new String[help.size() - 1]));
	}

	public void tryCommand(InternalCommand command, CommandSender sender, String label, String[] args) {
		boolean isPlayer = sender instanceof Player;
		try {
			switch (command.getUse()) {
			case PLAYER:
				if (isPlayer)
					command.run(sender, label, args);
				else
					plugin.sendErrorMessage(sender, "You need to be a player to use this command!");
				break;
			case CONSOLE:
				if (isPlayer)
					plugin.sendErrorMessage(sender, "This command can only be accessed through the console!");
				else
					command.run(sender, label, args);

				break;
			case BOTH:
					command.run(sender, label, args);
					
			}			 
		} catch (Exception e) {
			plugin.sendErrorMessage(sender, String.format("Improper usage of command '%s'!", command.getAliases()[0]));
			listHelp(sender, command);
			e.printStackTrace();
		}
	}

	private void listHelp(CommandSender sender, InternalCommand command) {
		for (String helpMessage : command.getHelp())
			plugin.sendNotificationMessage(sender, helpMessage);
	}

	public void loadCommand(InternalCommand command) {
		commands.add(command);
	}
}
