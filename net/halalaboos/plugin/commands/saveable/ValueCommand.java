package net.halalaboos.plugin.commands.saveable;

import org.bukkit.command.CommandSender;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.settings.Value;
import net.halalaboos.plugin.util.PluginUtils;

public class ValueCommand implements InternalCommand {

	private final BasePlugin plugin;
	
	private final Value value;
	
	public ValueCommand(BasePlugin plugin, Value value) {
		this.plugin = plugin;
		this.value = value;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { PluginUtils.fixedCommandName(value.getName()) };
	}

	@Override
	public String[] getHelp() {
		return new String[] { PluginUtils.fixedCommandName(value.getName()) + " <value>" };
	}

	@Override
	public String getDescription() {
		return value.getDescription();
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		if (PluginUtils.isDouble(args[1])) {
			double value = Double.parseDouble(args[1]);
			this.value.setValue(value);
		} else {
			plugin.sendErrorMessage(sender, "You must input a numerical value! Try \"/" + getHelp()[0] + "\"");
		}
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(plugin.getAdminPermissions());
	}

}
