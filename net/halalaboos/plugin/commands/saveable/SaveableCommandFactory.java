package net.halalaboos.plugin.commands.saveable;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.settings.*;

public final class SaveableCommandFactory {

	public static InternalCommand getCommand(BasePlugin plugin, Saveable saveable) {
		if (saveable instanceof Toggleable) {
			Toggleable toggleable = (Toggleable) saveable;
			return new ToggleableCommand(plugin, toggleable);
		} else if (saveable instanceof Value) {
			Value value = (Value) saveable;
			return new ValueCommand(plugin, value);
		} else {
			return null;
		}
	}
	
}
