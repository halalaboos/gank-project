package net.halalaboos.plugin.commands.saveable;

import org.bukkit.command.CommandSender;

import net.halalaboos.plugin.BasePlugin;
import net.halalaboos.plugin.commands.InternalCommand;
import net.halalaboos.plugin.settings.Toggleable;
import net.halalaboos.plugin.util.PluginUtils;

public class ToggleableCommand implements InternalCommand {

	private final BasePlugin plugin;
	
	private final Toggleable toggleable;
	
	public ToggleableCommand(BasePlugin plugin, Toggleable toggleable) {
		this.plugin = plugin;
		this.toggleable = toggleable;
	}
	
	@Override
	public String[] getAliases() {
		return new String[] { PluginUtils.fixedCommandName(toggleable.getName()) };
	}

	@Override
	public String[] getHelp() {
		return new String[] { PluginUtils.fixedCommandName(toggleable.getName()) + " <on/off>" };
	}

	@Override
	public String getDescription() {
		return toggleable.getDescription();
	}

	@Override
	public Use getUse() {
		return Use.BOTH;
	}

	@Override
	public void run(CommandSender sender, String label, String[] args) {
		String input = args[1];
		if (input.startsWith("on") || input.startsWith("en")) {
			toggleable.setEnabled(true);
			plugin.sendConfirmMessage(sender, toggleable.getName() + " is now enabled.");
		} else if (input.startsWith("off") || input.startsWith("dis")) {
			toggleable.setEnabled(false);
			plugin.sendConfirmMessage(sender, toggleable.getName() + " is now disabled.");
		}
	}

	@Override
	public boolean hasPermission(CommandSender sender) {
		return sender.hasPermission(plugin.getAdminPermissions());
	}

	public Toggleable getToggleable() {
		return toggleable;
	}

}
